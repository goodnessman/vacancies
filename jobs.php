<?php
  // session_start();
  //require_once('php/db.php');
  require_once('php/user.php');
  $user = new User();
  $user->RestoreUser();
  // print_r($user);
  // print_r($user->login);
?>
<!DOCTYPE html>
<html lang="ru">
  <head>
    <title>Vacancii</title>
    <meta charset="UTF-8">
    <!--meta(http-equiv='X-UA-Compatible', content='IE=edge')-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--style-->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="libs/bootstrap-3.3.7/css/bootstrap.min.css" >
    <!---->
    <script src="libs/jquery/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="libs/bootstrap-3.3.7/js/bootstrap.min.js"></script>
	  <script type="text/javascript" src="libs/bootstrap-3.3.7/js/bootstrap.js"></script>
  </head>
  <body>
    <div class="wrapper">
        <header class="main-header box8">
            <div class="container">
                <div class="main-header__wrap">
                    <button type="button" class="btn btn-primary" id="createjob">Создать вакансию</button>
                    <div class="main-header__right">
                        <ul class="main-login">
                            <!-- <li><a href="#logout"><i class="icon-user-clear"></i>Logout</a></li> -->
                            <?php
                             if($user->isActive()) {
                                echo '<li><a href="#profile"><span class="glyphicon glyphicon-user"></span> '.$user->GetLogin().'</a></li>';
                                echo '<li><a href="#logout"><i class="icon-user-clear"></i>Выход</a></li>';
                            }
                            //  else {
                            //     echo '<li><a href="#login"><i class="icon-user-clear"></i>Login</a></li>
                            //           <li><a href="#sign_up"><i class="icon-sign2"></i>Sign up</a></li>';
                            // }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
        <!--Main-->
        <div class="main">
            <div class="container">
                <div class="newjob">
                    <p class="urljobs">
                      <a data-toggle="collapse" href="#jobs_new" role="button" aria-expanded="false" aria-controls="collapseExample">Новая вакансия</a>
                    </p>
                    <div class="collapse jobs_new" id="jobs_new">
                      <div class="card card-body">
                        <form id="jobform" class="jobform">
                            <div class="form-group">
                                <input class="form-control" type="text" name="idjob" value="" style="display: none;">
                                <label for="input">Вакансия</label>
                                <input class="form-control" type="text" name="jobhead" placeholder="Вакансия">
                                <label for="input">Компания</label>
                                <input class="form-control" type="text" name="jobcompany" placeholder="Компания">
                                <label for="textarea">Описание вакансии</label>
                                <textarea class="form-control" name="jobcoment" rows="3"></textarea>
                                <div class="form-group">
                                    <label for="jobfile">Файл</label>
                                    <label class="custom-file jobfile" style="width: 100%;">
                                        <input type="file" class="form-control custom-file-input fileinput" name="file" id="file">
                                        <span class="custom-file-control" data-content-value="Выберите файл..."></span>
                                    </label>
                                </div>
                                <label for="input">Email для получения заявок</label>
                                <input class="form-control" type="email" name="email" placeholder="Email" value="">
                                <label for="input">Переадресация</label>
                                <input class="form-control" type="text" name="redirect_url" placeholder="https://...">
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-primary savejob">Сохранить</button>
                                </div>
                            </div>
                        </form>
                      </div>
                    </div>
                </div>

                <div class="jobslist">
                    <?php
                        echo $user->getJobs();
                    ?>
                </div>
            </div>
        </div>
        <!--End Main-->
    </div>
  </body>
</html>
<script src="js/script.js"></script>