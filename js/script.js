let path = '';
let error_registr = 0;
let reg_id_input = ['namereg','secondnamereg','emailregistr','passregistr','confirmpass'];
let count_success_quetion = 0;
let obj_questions;
let count_quetion = 0;
let number_question = 1;
let ansver_q;
let vakanciya_id;
let count_new_ach = 1;
let count_achievements, motivation;

$(document).ready(function () {

    if (window.location.pathname.indexOf('web_admin')!=-1) { path = '../'; }

    $('.newjob').hide();

    $('#send_achievements').on('click', function () {
        sendAchievements(true);
        $('#add_achievements_has').show();
        $('#add_achievements').hide();
        count_achievements++;
        // count_achievements++;
        // if (motivation>0) {
        //     $('#sendorder').attr('disabled',false);
        // }
        return;
    });

    $('#add_achievements_item').on('click', function () {
        $('#achievements').append(
            '<div class="col-lg-12 achievements_block achievements_item" style="margin-top: 25px;">'+
                '<div class="col-lg-4 achievements_block">'+
                    '<label class="" for="inputEmail">Общая информация</label>'+
                    '<input type="role" name="role" class="form-control role" placeholder="Твоя роль" autofocus="true">'+
                    '<div class="col-lg-6 achievements_block">'+
                        '<select type="text" class="dop_info_select form-control start_month color_place_holder" name="start_month">'+
                            '<option value="Месяц" class="white_color">Месяц</option>'+
                            '<option value="Январь" class="black_color">Январь</option>'+
                            '<option value="Февраль" class="black_color">Февраль</option>'+
                            '<option value="Март" class="black_color">Март</option>'+
                            '<option value="Апрель" class="black_color">Апрель</option>'+
                            '<option value="Май" class="black_color">Май</option>'+
                            '<option value="Июнь" class="black_color">Июнь</option>'+
                            '<option value="Июль" class="black_color">Июль</option>'+
                            '<option value="Август" class="black_color">Август</option>'+
                            '<option value="Сентябрь" class="black_color">Сентябрь</option>'+
                            '<option value="Октябрь" class="black_color">Октябрь</option>'+
                            '<option value="Ноябрь" class="black_color">Ноябрь</option>'+
                            '<option value="Декабрь" class="black_color">Декабрь</option>'+
                        '</select>'+
                        '<select type="text" class="dop_info_select form-control end_month color_place_holder" name="end_month">'+
                            '<option value="Месяц" class="white_color">Месяц</option>'+
                            '<option value="Январь" class="black_color">Январь</option>'+
                            '<option value="Февраль" class="black_color">Февраль</option>'+
                            '<option value="Март" class="black_color">Март</option>'+
                            '<option value="Апрель" class="black_color">Апрель</option>'+
                            '<option value="Май" class="black_color">Май</option>'+
                            '<option value="Июнь" class="black_color">Июнь</option>'+
                            '<option value="Июль" class="black_color">Июль</option>'+
                            '<option value="Август" class="black_color">Август</option>'+
                            '<option value="Сентябрь" class="black_color">Сентябрь</option>'+
                            '<option value="Октябрь" class="black_color">Октябрь</option>'+
                            '<option value="Ноябрь" class="black_color">Ноябрь</option>'+
                            '<option value="Декабрь" class="black_color">Декабрь</option>'+
                        '</select>'+
                        // '<input type="start_month" name="start_month" class="form-control start_month" placeholder="Месяц">'+
                        // '<input type="end_month" name="end_month" class="form-control end_month" placeholder="Месяц">'+
                    '</div>'+
                    '<div class="col-lg-6 achievements_block">'+
                        '<select class="dop_info_select form-control start_year color_place_holder" name="start_year">'+
                            '<option value="Год начала" class="white_color">Год начала</option>'+
                            '<option value="2020" class="black_color">2020</option>'+
                            '<option value="2021" class="black_color">2021</option>'+
                            '<option value="2022" class="black_color">2022</option>'+
                            '<option value="2023" class="black_color">2023</option>'+
                            '<option value="2024" class="black_color">2024</option>'+
                            '<option value="2025" class="black_color">2025</option>'+
                            '<option value="2026" class="black_color">2026</option>'+
                            '<option value="2027" class="black_color">2027</option>'+
                            '<option value="2028" class="black_color">2028</option>'+
                            '<option value="2029" class="black_color">2029</option>'+
                            '<option value="2030" class="black_color">2030</option>'+
                        '</select>'+
                        '<select class="dop_info_select form-control end_year color_place_holder" name="end_year">'+
                            '<option value="Год окончания" class="white_color">Год окончания</option>'+
                            '<option value="2020" class="black_color">2020</option>'+
                            '<option value="2021" class="black_color">2021</option>'+
                            '<option value="2022" class="black_color">2022</option>'+
                            '<option value="2023" class="black_color">2023</option>'+
                            '<option value="2024" class="black_color">2024</option>'+
                            '<option value="2025" class="black_color">2025</option>'+
                            '<option value="2026" class="black_color">2026</option>'+
                            '<option value="2027" class="black_color">2027</option>'+
                            '<option value="2028" class="black_color">2028</option>'+
                            '<option value="2029" class="black_color">2029</option>'+
                            '<option value="2030" class="black_color">2030</option>'+
                        '</select>'+
                        // '<input type="start_year" name="start_year" class="form-control start_year" placeholder="Год начала">'+
                        // '<input type="end_year" name="end_year" class="form-control end_year" placeholder="Год окончания">'+
                    '</div>'+

                '</div>'+
                '<div class="col-lg-4" style="padding-left: 30px;">'+
                    '<label class="">Твои задачи</label>'+
                    '<textarea class="form-control task" rows="6" spellcheck="false" data-gramm="false" placeholder="Введите текст"></textarea>'+
                '</div>'+
                '<div class="col-lg-4">'+
                    '<label class="">Результаты</label>'+
                    '<textarea class="form-control results" rows="5" spellcheck="false" data-gramm="false" placeholder="Введите текст"></textarea>'+
                '</div>'+
            '</div>'
        );
        $('#del_achievements_item').show();
        count_new_ach++;
        $('.dop_info_select').on('change', function () {
            // console.log($(this));
            if ($(this)[0].selectedIndex!=0) {
                $(this).removeClass("color_place_holder");
            }else{
                if (!$(this).hasClass('color_place_holder')) {
                    $(this).addClass("color_place_holder");
                }
            }
            return;
        });
        return;
    });
    
    $('#del_achievements_item').on('click', function () {
        if (count_new_ach==1) {$('#del_achievements_item').hide(); return;}
        var parent = document.getElementById("achievements");
        parent.removeChild(parent.lastElementChild);
        count_new_ach--;
        if (count_new_ach==1) {$('#del_achievements_item').hide();}
        sendAchievements();
        return;
    });

    $('#backtoorder_achi').on('click', function () {
        backToOrder('collg6c_achievements');
        return;
    });

    $('#add_achievements_has').on('click', function () {
        addAchievements();
        return;
    });

    $('#add_achievements').on('click', function () {
        addAchievements();
        return;
    });

    $('#add_achievements_modal').on('click', function () {
        console.log('getUserAchievements');
        getUserAchievementsModal();
        return;
    });
    $('#add_achievements_has_modal').on('click', function () {
        getUserAchievementsModal();
        return;
    });

    $('#send_motivation').on('click', function () {
        var motivation = $('#motivation').val();
        console.log('motivation',motivation);
        var url = path+"php/ajax.php?action=addMotivation";
        $.post(url, { motivation: motivation, vakanciya_id:vakanciya_id }, function(data){
            var resp = JSON.parse(data);
            if(resp['success'] === true) {
                $('.mess_err_send').text('');
                // motivation++;
                // if (count_achievements>0) {
                //     $('#sendorder').attr('disabled',false);
                // }
                $('#add_motivation').hide();
                $('#add_motivation_has').show();
                backToOrder('collg6c_motivation');
            } else {
                $('.mess_err_send').text(resp['message']);
            }
        });
        return;
    });

    $('#modal_send_motivation').on('click', function () {
        var motivation = $('#modal_motivation').val();
        var url = path+"php/ajax.php?action=addMotivation";
        $.post(url, { motivation: motivation, vakanciya_id:vakanciya_id }, function(data){
            var resp = JSON.parse(data);
            if(resp['success'] === true) {
                $('.mess_err_send').text('');
                $('#add_motivation_modal').hide();
                $('#add_motivation_has_modal').show();
                // backToOrder('collg6c_motivation');
            } else {
                $('.mess_err_send').text(resp['message']);
            }
        });
        return;
    });

    $('#backtoorder').on('click', function () {
        backToOrder('collg6c_motivation');
        return;
    });

    $('#add_motivation_has').on('click', function () {
        addMotivation();
        return;
    });

    $('#add_motivation').on('click', function () {
        addMotivation();
        return;
    });

    $('#add_motivation_modal').on('click', function () {
        getUserMotivation();
        return;
    });
    $('#add_motivation_has_modal').on('click', function () {
        getUserMotivation();
        return;
    });

    $('#hide_block5').on('click', function () {
        $('#add_achievements').attr('disabled',true);
        $('#add_achievements_has').attr('disabled',true);
        $('#add_motivation').attr('disabled',true);
        $('#add_motivation_has').attr('disabled',true);
        $('.dop_info').addClass("disable");
        $('.block5').addClass("disable");
        $('#sendorder').attr('disabled',false);
        return;
    });
    
    $('#notesting').on('click', function () {
        $('#testing').hide();
        $('#notesting').hide();
        $('#retesting').show();
        return;
    });

    $('#exitquery').on('click', function () {
        $('.collg6l').removeClass("revealator-slideright");
        $('.collg6l').removeClass("revealator-delay2");
        $('.collg6l').removeClass("revealator-once");
        $('.collg6l').removeClass("revealator-within");
        $('.collg6r').removeClass("revealator-slideright");
        $('.collg6r').removeClass("revealator-delay2");
        $('.collg6r').removeClass("revealator-once");
        $('.collg6r').removeClass("revealator-within");
        $('.collg6c').addClass("revealator-slideright");
        $('.collg6c').addClass("revealator-delay2");
        $('.collg6c').addClass("revealator-once");
        setTimeout (function() {
            $('.collg6c').hide();
            $('.collg6l').show();
            $('.collg6r').show();
            $('#testing').hide();
            $('#notesting').hide();
            $('#retesting').show();
        }, 800);
        sendAnswerQuestion(count_success_quetion);
        return;
    });

    $('#backquery').on('click', function () {
        if (number_question==2) {
            $('#backquery').attr('disabled',true);
            return;
        }
        number_question = number_question - 2;
        showQuestion();
        return;
    });

    $('#nextquery').on('click', function () {
        $('#backquery').attr('disabled',false);
        $("input[name='answer[]']:checked").each(function() {
            for (var i = 0; i < ansver_q.length; i++) {
                if ($(this).val()==ansver_q[i]) {
                    count_success_quetion++;
                }
            }
        });
        showQuestion();
        return;
    });

    $('#retesting').on('click', function () {
        count_success_quetion = 0;
        number_question = 1;
        executeTest();
        return;
    });

    $('#testing').on('click', function () {
        executeTest();
        return;
    });

    $('#sendorder').on('click', function () {
        var job_url = window.location.search.split('url=');
        job_url = job_url[1];
        var url = path+"php/ajax.php?action=sendorder";
        // console.log('job_url', job_url);
        $.post(url, { job_url: job_url }, function(data){
            var resp = JSON.parse(data);
            // console.log(resp);
            if(resp['success'] === true) {
                $('.mess_err_send').text('');
                // window.location.href = path+'success_send_order.php';
                window.location.href = resp['redirect_url'];
            } else {
                $('.mess_err_send').text(resp['message']);
            }
        });
        return;
    });

    $("#resumefileinput").change(function () {
        var data = new FormData();
        var callback = checkFilesLoad('resumefileinput', data);
        data.append( 'action', 'file_upload_resume' );
        $.ajax({
            url         : 'php/ajax.php',
            type        : 'POST', // важно!
            data        : data,
            cache       : false,
            dataType    : 'text',
            // отключаем обработку передаваемых данных, пусть передаются как есть
            processData : false,
            // отключаем установку заголовка типа запроса. Так jQuery скажет серверу что это строковой запрос
            contentType : false, 
            // функция успешного ответа сервера
            success     : function( respond, status, jqXHR ){
                // console.log('respond',respond);
                location.reload();
            }
        });
        return false;
    });

    if (document.getElementById("ModalRegistr")) {
        document.getElementById("ModalRegistr").onclick = function(event) {
            if (event.target === this) {
                clearErrors();
            }
        };
        document.getElementById("ModalLogin").onclick = function(event) {
            if (event.target === this) {
                clearErrors();
            }
        };
    }
	
    $("#loginVK").click(function () {
        clearErrors();
        console.log($(this));
        sendAjax('loginVK', function () {
            // window.location.href = 'web_admin/index.php';
            // window.location.reload();
        });
    });

    $("button.close").click(function () {
        clearErrors();
    });

    $(".ModalRegistr").click(function () {
        clearErrors();
    });

    $("#registr").click(function () {
        clearErrors();
        for (var i = 0; i < reg_id_input.length; i++) {
            checkfieldempty(reg_id_input[i]);
        }
        if (error_registr>0) {return false;}
        if($("#emailregistr").val().search(/^((([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z\s?]{2,5}){1,25})*(\s*?;\s*?)*)*$/)<0){
            $('.mess_err').text('E-mail введен некорректно');
            return false;
        }
        if ($('#passregistr').val().length==0) {
            $('.mess_err').text('Поле пароль не заполнено');
            return false;
        }
        checkfield('confirmpass');
        checkfield('passregistr');

        if (error_registr>0) {
            $('.mess_err').text('Не все данные введенны корректно');
            return false;
        }
        sendAjaxForm('registrform', 'registr', function (resp) {
            if(resp['success'] === true) {
                sendAjaxForm('registrform', 'login', function (resp) {
                    if(resp['success'] === true) {
                        $('.mess_err').text('');
                        if ( (resp['role']==1) && (window.location.pathname.indexOf('web_admin')!=-1) ) {
                            clearErrors();
                            window.location.href = path+'jobs.php';
                        } else {
                            location.reload();
                        }
                    } else {
                        $('.mess_err').text(resp['message']);
                    }
                });
            } else {
                $('.mess_err').text(resp['message']);
            }
        });
        return false;
    });

	$("#login").click(function () {
        sendAjaxForm('signin', 'login', function (resp) {
            if(resp['success'] === true) {
            	$('.mess_err').text('');
                if ( (resp['role']==1) && (window.location.pathname.indexOf('web_admin')!=-1) ) {
                    clearErrors();
                    window.location.href = path+'jobs.php';
                }else{
                    location.reload();
                }
            } else {
                $('.mess_err').text(resp['message']);
            }
        });
        return false;
    });

    $("#createjob").click(function () {
    	$('.newjob').show();
        // $('div[id="#jobs_new"]').css("display","block");
        $('.jobs_new').addClass("in");
    	return false;
    });

    $(".fileinput").change(function () {
    	$(this).next().attr('data-content-value', $(this)[0].files[0].name);
    	return false;
    });

    $(".jobfileinput").change(function () {
        $(this).next().attr('data-content-value', $(this)[0].files[0].name);
        var res = $(this)[0].files[0].name.split(".");
        // console.log('res',res);
        if(res[1].search(/png|jpg/)<0){
            alert('Некорректный формат файла логотипа. Допустимые форматы: png, jpg, jpeg');
            $(this).next().attr('data-content-value', 'Выберите файл...');
            $(this).val('');
        }
        return false;
    });

    $(document).on('focusout','#confirmpass',function(e){
        var id = $(this).attr("id");
        checkfield(id);
    });

    $(document).on('focusout','#passregistr',function(e){
        var id = $(this).attr("id");
        checkfield(id);
    });

    $(".savejob").click(function () {
    	let form = $(this).parent().parent().parent();
    	let id = form[0].id;
    	sendAjaxForm(id, 'savejob', function (resp) {
        	console.log('resp',resp);
            if(resp['success'] === true) {
                var idjob = resp['idjob'];
                if (resp['idnewjob']!=0) {
                    idjob = resp['idnewjob'];
                }
                var data = new FormData();
                var callback = checkFilesLoad('file'+resp['idjob'], data);
                data.append( 'action', 'file_upload' );
                data.append( 'idjob', idjob );
                $.ajax({
                    url         : 'php/ajax.php',
                    type        : 'POST', // важно!
                    data        : data,
                    cache       : false,
                    dataType    : 'text',
                    // отключаем обработку передаваемых данных, пусть передаются как есть
                    processData : false,
                    // отключаем установку заголовка типа запроса. Так jQuery скажет серверу что это строковой запрос
                    contentType : false, 
                    // функция успешного ответа сервера
                    success     : function( respond, status, jqXHR ){
                        console.log('respond',respond);
                        location.reload();
                    }
                });
            } else {
                $('.mess_err').text(resp['message']);
            }
        });
    	return false;
    });

    $(".deljob").click(function () {
        let form = $(this).parent().parent().parent();
        let id = form[0].id;
        sendAjaxForm(id, 'deljob', function (resp) {
            if(resp['success'] === true) {
                location.reload();
            } else {
                $('.mess_err').text(resp['message']);
            }
        });
        return false;
    });

    $('a[href="#logout"]').on('click', function () {
        $('.mess_err').empty();
        sendAjax('logout', function () {
        	window.location.href = 'web_admin/index.php';
            // window.location.reload();
        });
    });

});

function addMotivation(){
    console.log('addMotivation');
    getUserMotivation();
    $('.collg6c_motivation').removeClass("revealator-slideright");
    $('.collg6c_motivation').removeClass("revealator-delay2");
    $('.collg6c_motivation').removeClass("revealator-once");
    $('.collg6c_motivation').removeClass("revealator-within");
    $('.collg6l').addClass("revealator-slideright");
    $('.collg6l').addClass("revealator-delay2");
    $('.collg6l').addClass("revealator-once");
    $('.collg6r').addClass("revealator-slideright");
    $('.collg6r').addClass("revealator-delay2");
    $('.collg6r').addClass("revealator-once");
    setTimeout (function() {
        $('.collg6l').hide();
        $('.collg6r').hide();
        $('.collg6c_motivation').show();
    }, 800);
}

function addAchievements(){
    getUserAchievements();
    $('.collg6c_achievements').removeClass("revealator-slideright");
    $('.collg6c_achievements').removeClass("revealator-delay2");
    $('.collg6c_achievements').removeClass("revealator-once");
    $('.collg6c_achievements').removeClass("revealator-within");
    $('.collg6l').addClass("revealator-slideright");
    $('.collg6l').addClass("revealator-delay2");
    $('.collg6l').addClass("revealator-once");
    $('.collg6r').addClass("revealator-slideright");
    $('.collg6r').addClass("revealator-delay2");
    $('.collg6r').addClass("revealator-once");
    setTimeout (function() {
        $('.collg6l').hide();
        $('.collg6r').hide();
        $('.collg6c_achievements').show();
    }, 800);
}

function sendAchievements(close_forme=false){
    let achievements_items = {};
    let achievements = $('#achievements');
    achievements.find(".achievements_item").each(function(index){ 
        achievements_items[index] = {};
        achievements_items[index].id_item = $(this).find('.id_item').val();
        achievements_items[index].role = $(this).find('.role').val();
        achievements_items[index].start_year = $(this).find('.start_year').val();
        achievements_items[index].end_year = $(this).find('.end_year').val();
        achievements_items[index].start_month = $(this).find('.start_month').val();
        achievements_items[index].end_month = $(this).find('.end_month').val();
        achievements_items[index].task = $(this).find('.task').val();
        achievements_items[index].results = $(this).find('.results').val();
    });
    // console.log('achievements_items',achievements_items);
    var url = path+"php/ajax.php?action=addAchievements";
    $.post(url, { achievements_items: achievements_items, vakanciya_id:vakanciya_id }, function(data){
        var resp = JSON.parse(data);
        if(resp['success'] === true) {
            $('.mess_err_send').text('');
            if (close_forme) { backToOrder('collg6c_achievements');$('#sendorder').attr('disabled',false); }
        } else {
            $('.mess_err_send').text(resp['message']);
        }
    });
}

function getUserAchievementsModal(){
    var url = path+"php/ajax.php?action=getUserAchievementsModal";
    $.post(url, {vakanciya_id:vakanciya_id}, function(data){
        var resp = JSON.parse(data);
        console.log('resp',resp);
        if(resp['success'] === true) {
            count_new_ach = resp['count'];
            if (count_new_ach>0) {
                for (var i = 0; i<resp['data'].length; i++) {
                    console.log('role',resp['data'][i]['role']);
                    $('.role_1').val(resp['data'][i]['role']);
                }
            }
        }
    });
}

function getUserAchievements(){
    var url = path+"php/ajax.php?action=getUserAchievements";
    $.post(url, {vakanciya_id:vakanciya_id}, function(data){
        var resp = JSON.parse(data);
        if(resp['success'] === true) {
            $('#achievements').empty();
            $('#achievements').append(resp['block']);
            count_new_ach = resp['count'];
            if (count_new_ach>1) {$('#del_achievements_item').show();}
            $('.dop_info_select').on('change', function () {
                // console.log($(this));
                if ($(this)[0].selectedIndex!=0) {
                    $(this).removeClass("color_place_holder");
                }else{
                    if (!$(this).hasClass('color_place_holder')) {
                        $(this).addClass("color_place_holder");
                    }
                }
                return;
            });
        }
    });
}

function getUserMotivation(){
    console.log('getUserMotivation');
    var url = path+"php/ajax.php?action=getUserMotivation";
    $.post(url, {vakanciya_id:vakanciya_id}, function(data){
        var resp = JSON.parse(data);
        console.log('resp',resp);
        if(resp['success'] === true) {
            $('#motivation').text(resp['motivation']);
            $('#modal_motivation').text(resp['motivation']);
        }
    }); 
}

function backToOrder(from_div){
    $('.collg6l').removeClass("revealator-slideright");
    $('.collg6l').removeClass("revealator-delay2");
    $('.collg6l').removeClass("revealator-once");
    $('.collg6l').removeClass("revealator-within");
    $('.collg6r').removeClass("revealator-slideright");
    $('.collg6r').removeClass("revealator-delay2");
    $('.collg6r').removeClass("revealator-once");
    $('.collg6r').removeClass("revealator-within");
    $('.'+from_div).addClass("revealator-slideright");
    $('.'+from_div).addClass("revealator-delay2");
    $('.'+from_div).addClass("revealator-once");
    setTimeout (function() {
        $('.'+from_div).hide();
        $('.collg6l').show();
        $('.collg6r').show();
        $('#achievements').empty();
    }, 800);
}

function sendAnswerQuestion(count_success_quetion){
    var url = path+"php/ajax.php?action=sendAnswerQuestion";
    $.post(url, {count_success_quetion:count_success_quetion}, function(data){
        var resp = JSON.parse(data);
    });
}

function executeTest(){
    $('.collg6c').removeClass("revealator-slideright");
    $('.collg6c').removeClass("revealator-delay2");
    $('.collg6c').removeClass("revealator-once");
    $('.collg6c').removeClass("revealator-within");
    $('.collg6l').addClass("revealator-slideright");
    $('.collg6l').addClass("revealator-delay2");
    $('.collg6l').addClass("revealator-once");
    $('.collg6r').addClass("revealator-slideright");
    $('.collg6r').addClass("revealator-delay2");
    $('.collg6r').addClass("revealator-once");
    setTimeout (function() {
        showQuestion();
        $('#nextquery').show();
        $('#backquery').show();
        $('#exitquery').hide();
        $('.collg6l').hide();
        $('.collg6r').hide();
        $('.collg6c').show();
    }, 800);
}

function showQuestion(){
    $('.answer-style').empty();
    $('.countq').text(number_question+'/'+count_quetion);
    if (!obj_questions[number_question]) {
        $('#nextquery').hide();
        $('#backquery').hide();
        $('#exitquery').show();
        return;
    }
    if (number_question==count_quetion) {
        $('#nextquery').hide();
        $('#backquery').hide();
        $('#exitquery').show();
    }
    let title_q = obj_questions[number_question].title;
    let type_q = obj_questions[number_question].type;
    let variantsCount_q = obj_questions[number_question].variantsCount;
    let variants_q = String(obj_questions[number_question].variants).split(";;");
    ansver_q = String(obj_questions[number_question].ansver).split(";;");
    // console.log('title_q',title_q,'type_q',type_q,'variantsCount_q',variantsCount_q,'ansver_q',ansver_q,'variants_q',variants_q);
    $('.qtitle').text(title_q);
    if (type_q == 'variant') {
        for (var i = 0; i < variants_q.length; i++) {
            var el = '<p class="pclassq">'+
                '<label class="custom-control custom-checkbox">'+
                    '<input name="answer[]" value="'+variants_q[i]+'" type="checkbox" class="custom-control-input qinput">'+
                    '<span class="custom-control-indicator"></span>'+
                    '<span class="custom-control-description">'+variants_q[i]+'</span>'+
                '</label>'+
            '</p>';
            $('.answer-style').append(el);
        }
    }
    if (ansver_q.length==1) {
        $(".qinput").click(function () {
            $('input[class="custom-control-input qinput"]').removeAttr("checked");
            $(this).prop("checked", true);
            // if ($(this).is(':checked')) {
            //     $('input[class="custom-control-input qinput"]').trigger("click");
            // }else{
            //     $('input[class="custom-control-input qinput"]').removeAttr("checked");
            // }
            return;
        });
    }
    number_question++;
    return;
}

function getJsonObjTestQuestions(){
    var url = path+"php/getQuestions.php";
    $.post(url, { }, function(data){
        if (!data) {return;}
        var resp = JSON.parse(data);
        console.log('resp',resp);
        if(resp['success'] === true) {
            obj_questions = resp['array_parse'];
            count_quetion = resp['count_question'];
        }
    });
}

function clearErrors(){
    $('.mess_err').text('');
    error_registr = 0;
    for (var i = 0; i < reg_id_input.length; i++) {
        delbrcololr(reg_id_input[i]);
    }
}

function checkfieldempty(id){
    delbrcololr(id);
    $('.mess_err').text('');
    if ($('#'+id).val().length==0) {
        $('.mess_err').text('Не все поля заполнены');
        brcololr(id);
        error_registr++;
    }
}

function brcololr(id){
    $('#'+id).css("border-color","#e51b24");
}

function delbrcololr(id){
    $('#'+id).css("border-color","#cccccc");
}

function checkfield(id){
    $('.mess_err').text('');
    delbrcololr(id);
    if (id=='confirmpass') {
        if ($('#passregistr').val().length!=0) {
            if ($('#confirmpass').val().length==0) {
                $('.mess_err').text('Поле подтверждение пароля не заполнено');
                error_registr++;
            }else{
                if ($("#passregistr").val()!=$("#confirmpass").val() ){
                    $('.mess_err').text('Пароли не совпадают');
                    error_registr++;
                }
            }
        }
    }
    if (id=='passregistr') {
        if ($('#passregistr').val().length!=0) {
            if ($('#confirmpass').val().length!=0) {
                if ($("#passregistr").val()!=$("#confirmpass").val() ){
                    $('.mess_err').text('Пароли не совпадают');
                    error_registr++;
                }
            }
        }
    }
}

function checkFilesLoad(id, data){
    var control = document.getElementById(id);
    if (!control) {return false}
    var files = control.files,
    len = files.length;
    if( len == 0 ){ return false; }
    var filename = files[0].name;
    // console.log('filename',filename,'len',len);
    // заполняем объект данных файлами в подходящем для отправки формате
    $.each( files, function( key, value ){
        data.append( id, value );
    });
    return true;
}

function sendAjax(params_url, callback, c_error) {
    $.ajax({
        url: path+"php/ajax.php?action=" + params_url,
        type: "POST",
        dataType: 'json',
        success: function (str) {
            // console.log(str);
            if(typeof callback === "function") callback(str);
        },
        error: function (e) {
            if(typeof c_error === "function") c_error(e);
        }
    });
}

function sendAjaxForm(ajax_form, params_url, callback, c_error) {
    let url = path+"php/ajax.php?action=" + params_url;
    $.ajax({
        url: url,
        type: "POST",
        dataType: 'json',
        cache: false,
        data: $("#" + ajax_form).serialize(),
        success: function (str) {
            // console.log('str',str);
            if(typeof callback === "function") callback(str);
            /*
            if (str.success == true) {
                // alert(str.message);
                // $('.mess').html(str.message);
                switch (str.role) {
                    case 1:
                        window.location.href = 'index.php';
                        break;
                    case 2:
                        window.location.href = 'employer-start.php';
                        break;
                    case 3:
                        window.location.href = 'candidate-start.php';
                        break;
                    case 4:
                        window.location.href = 'employer-start.php';
                        break;
                }
            } else {
                // alert(str.message);
                $('.mess_err').text(str.message);
                // return false;
            }
            */
        },
        error: function (e) {
            if(typeof c_error === "function") c_error(e);
        }
    });
}