<!DOCTYPE html>
<html lang="ru">
  <head>
    <title>Form</title>
    <meta charset="UTF-8">
    <!--meta(http-equiv='X-UA-Compatible', content='IE=edge')-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--style-->
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../libs/bootstrap-3.3.7/css/bootstrap.min.css" >
    <!---->
    <script src="../libs/jquery/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="../libs/bootstrap-3.3.7/js/bootstrap.min.js"></script>
	  <script type="text/javascript" src="../libs/bootstrap-3.3.7/js/bootstrap.js"></script>
  </head>
  <body>
    <div class="wrapper">
      <header class="main-header">
        <div class="container">
          <div class="main-header__wrap">
            <div class="main-header__right">
              <ul class="main-login">
              </ul>
            </div>
          </div>
        </div>
        </header>
        <!--Main-->
        <div class="main">
            <form class="form-signin" id="signin">
                <div class="form-label-group">
                  <label for="inputEmail">Email</label>
                  <input type="email" id="inputEmail" name="login" class="form-control" placeholder="Email" required="" autofocus="true">
                </div>

                <div class="form-label-group">
                  <label for="inputPassword">Пароль</label>
                  <input type="password" id="inputPassword" name="pass" class="form-control" placeholder="Пароль" required="">
                </div>

                <div class="mess_err"></div>
                <!--<div class="checkbox mb-3">
                  <label>
                    <input type="checkbox" value="remember-me"> Remember me
                  </label>
                </div> -->
                <button class="btn btn-lg btn-primary btn-default btnsubmit" id="login" type="submit">Войти</button>
            </form>
        </div>
        <!--End Main-->
    </div>
  </body>
</html>
<script src="../js/script.js"></script>