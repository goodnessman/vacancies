/*
SQLyog Ultimate v9.60 
MySQL - 5.5.5-10.1.37-MariaDB : Database - vakancii
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`id12672989_localhosting` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `id12672989_localhosting`;

/*Table structure for table `import_files` */

DROP TABLE IF EXISTS `import_files`;

CREATE TABLE `import_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `origin_file_name` varchar(255) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `id_job` int(11) DEFAULT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `import_files` */

LOCK TABLES `import_files` WRITE;

insert  into `import_files`(`id`,`origin_file_name`,`file_name`,`id_job`,`date_create`) values 
(1, '1тест.txt', '1_20200224163641.txt', 1, '2020-02-24 16:36:41'),
(2, '2тест.txt', '1_20200228130030.txt', 1, '2020-02-28 13:00:30'),
(3, '1тест.txt', '2_20200228130055.txt', 2, '2020-02-28 13:00:55'),
(4, '20200206_204855.jpg', '3_20200229055516.jpg', 3, '2020-02-29 05:55:16'),
(5, '20200206_204855.jpg', '2_20200304104815.jpg', 2, '2020-03-04 10:48:15'),
(6, 'eye-512.png', '1_20200304160216.png', 1, '2020-03-04 16:02:16'),
(7, 'CROC_Incorporated.png', '2_20200307172715.png', 2, '2020-03-07 17:27:15');;

UNLOCK TABLES;

/*Table structure for table `jobs` */

DROP TABLE IF EXISTS `jobs`;

CREATE TABLE `jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `head` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `file` varchar(55) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `delete` int(3) DEFAULT '0',
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `jobs` */

LOCK TABLES `jobs` WRITE;

insert  into `jobs`(`id`,`head`,`description`,`file`,`url`,`date_create`,`delete`,`email`) values 
(1, 'Дизайнер', 'вакансия 1', '6', NULL, '2020-02-24 07:19:55', 0, 'mixpack@yandex.ru'),
(2, 'Помощник менеджера по корпоративной культуре', 'Мы ищем креативного менеджера, который готов создавать с нами нестандартные и эффективные HR проекты для решения задач бизнеса.', '7', NULL, '2020-02-24 07:24:35', 0, 'ph.kirilltsvetkov@yandex.ru'),
(3, 'test', 'test', '4', NULL, '2020-02-29 05:55:15', 0, 'mixpack@yandex.ru');

UNLOCK TABLES;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `role` int(3) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `patronymic` varchar(255) DEFAULT NULL,
  `date_registr` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_login` datetime DEFAULT '0001-01-01 00:00:00',
  `hash_email` varchar(255) DEFAULT NULL,
  `resume_file` varchar(255) DEFAULT NULL,
  `resume_file_origin` varchar(255) DEFAULT NULL,
  `testing_ans` int(3) DEFAULT NULL COMMENT 'количество верных ответов по тесту',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `users` */

LOCK TABLES `users` WRITE;

insert  into `users`(`id`,`login`,`password`,`email`,`role`,`name`,`surname`,`patronymic`,`date_registr`,`date_login`,`hash_email`,`resume_file`,`resume_file_origin`,`testing_ans`) values 
(1,'client@gmail.com','e10adc3949ba59abbe56e057f20f883e','client@gmail.com',1,'Aleksandr','','','2020-02-24 09:09:06','2020-03-04 23:41:49','12399f315adaa4c4fcdfabaafc900c7a','1_Resume.txt','Resume.txt', NULL),
(6, 'test@gmail.com', '098f6bcd4621d373cade4e832627b4f6', 'test@gmail.com', 0, 'test', 'test', NULL, '2020-02-27 16:18:33', '2020-03-04 10:42:15', '1aedb8d9dc4751e229a335e371db8058', NULL, NULL, NULL),
(7, 'mixpack@yandex.ru', '4b61d580dc74b0efd8c87238b74ff266', 'mixpack@yandex.ru', 0, 'Кирилл', 'Цветков', NULL, '2020-02-27 16:21:14', '2020-03-07 17:25:57', '761da1d7860d9df080afe646f55207cb', '7_BG-9.png', 'BG-9.png', NULL),
(8, 'nikita.pikovets@gmail.com', '098f6bcd4621d373cade4e832627b4f6', 'nikita.pikovets@gmail.com', 0, 'test', 'test', NULL, '2020-02-29 05:56:46', '2020-02-29 05:56:47', 'd7da7f8129abd9b6d56b226ac9a8a48d', '8_55a962831300002f009d7aae-jpeg__605.jpg', '55a962831300002f009d7aae-jpeg__605.jpg', NULL),
(9, 'nik.adrenalin@gmail.com', '098f6bcd4621d373cade4e832627b4f6', 'nik.adrenalin@gmail.com', 0, 'test', 'test', NULL, '2020-02-29 06:01:53', '2020-02-29 06:03:18', 'f9cdc4411e59672fde864de770a20c76', NULL, NULL, NULL),
(10, 'test2@gmail.com', '098f6bcd4621d373cade4e832627b4f6', 'test2@gmail.com', 0, 'test test', 'test', NULL, '2020-02-29 06:04:54', '2020-03-04 10:42:22', '3c4f419e8cd958690d0d14b3b89380d3', '10_image_0 (1).jpg', 'image_0 (1).jpg', NULL),
(12, 'teslenkosasha1992@mail.ru', '67fa7d0c779e7a322a4486415218627f', 'teslenkosasha1992@mail.ru', 0, 'Александр', 'Тесленко', NULL, '2020-02-29 15:43:49', '2020-02-29 19:58:20', 'e3d5a6d24c7cf631f72fb4db2a20fe0f', '12_Resume.txt', 'Resume.txt', NULL),
(14, 'regenun1@gmail.com', '9970f923c15edb4ef6ea04f142eda4db', 'regenun1@gmail.com', 0, 'Кирилл', 'Цветков', NULL, '2020-02-29 22:57:06', '2020-03-22 09:56:49', '41aa65f9351747c436d4fff7206ad0bd', '14_CROC_Incorporated.png', 'CROC_Incorporated.png', 0),
(15, 'test22@gmail.com', '098f6bcd4621d373cade4e832627b4f6', 'test22@gmail.com', 0, 'test', 'test', NULL, '2020-03-04 10:42:30', '2020-03-05 17:07:12', 'a9d34db43cb3c247fd6b571f7da63991', '15_Screenshot_20.png', 'Screenshot_20.png', NULL),
(16, 'morel96@yandex.ru', '82cac6d3b95de60b45412307adb62b0e', 'morel96@yandex.ru', 0, 'Амир', 'Сараков', NULL, '2020-03-08 13:30:58', '2020-03-08 13:32:08', 'bac59c717e200d1fae1f94edf5b42ff4', '16_Сараков_ОП.docx', 'Сараков_ОП.docx', NULL),
(17, 'kawasakimargo@mail.ru', 'ee8b9a6e6b2ef019c6552a431cf4baa3', 'kawasakimargo@mail.ru', 0, 'Маргарита', 'Евсюкова', NULL, '2020-03-09 09:44:29', '2020-03-09 09:44:29', 'dae66ca1f77a6053f3d5932063d3e767', '17_Prilozhenie_1.docx', 'Prilozhenie_1.docx', NULL),
(18, 'amiggogo@gmail.com', '6351bf9dce654515bf1ddbd6426dfa97', 'amiggogo@gmail.com', 0, 'Амир', 'С', NULL, '2020-03-10 12:09:04', '2020-03-10 12:09:04', 'c4b90485084347bdec17fefa7b0c729d', '18_Заявление_ОП.doc', 'Заявление_ОП.doc', NULL),
(19, '', '1e77b85f69594cb38a461b12ecef8c08', '', 0, '353763227}}', 'fields', NULL, '2020-03-11 15:13:34', '2020-03-11 15:14:02', 'd41d8cd98f00b204e9800998ecf8427e', NULL, NULL, NULL),
(20, 'meshkov07101993@mail.ru', '68ea1dda088c244a8e345368979c9408', 'meshkov07101993@mail.ru', 0, 'Даниил', 'Мешков', NULL, '2020-03-17 14:14:09', '2020-03-17 14:14:09', '3e81a2c18e9967dcb17104589f804afd', '20_fdf.jpg', 'fdf.jpg', 0),
(21, '', 'd41d8cd98f00b204e9800998ecf8427e', '', 0, '}', 'fields', NULL, '2020-03-17 17:39:58', '2020-03-17 17:39:58', 'd41d8cd98f00b204e9800998ecf8427e', NULL, NULL, NULL);

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
