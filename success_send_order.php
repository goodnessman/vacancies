<?php
    require_once('php/user.php');
    $user = new User();
    $user->RestoreUser();
    if(!$user->isActive()){
        $disable = 'disable';
    }
?>
<!DOCTYPE html>
<html lang="ru">
  <head>
    <title>Form</title>
    <meta charset="UTF-8">
    <!--meta(http-equiv='X-UA-Compatible', content='IE=edge')-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--style-->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="libs/bootstrap-3.3.7/css/bootstrap.min.css" >
    <!---->
    <script src="libs/jquery/jquery-2.1.3.min.js"></script>
    <!-- <script type="text/javascript" src="libs/bootstrap-3.3.7/js/bootstrap.min.js"></script> -->
	  <script type="text/javascript" src="libs/bootstrap-3.3.7/js/bootstrap.js"></script>
  </head>
  <body>
    <div class="wrapper">
        <header class="main-header box8">

                <!-- <div class="container"> -->
                    <div class="main-header__wrap">
                        <img src="img/logotip.png" width="26" height="40" style="margin-left: 3%;">
                        <div class="main-header__right">
                            <ul class="main-login">
                                <?php
                                    if($user->isActive()){
                                        // echo '<li><a href="#profile"><span class="glyphicon glyphicon-user"></span> '.$user->GetLogin().'</a></li>';
                                        echo '<li><a href="#profile"><img src="img/user.png" width="40" height="40" style=""> '.$user->GetFIO().'</a></li>';
                                    }else{
                                        // echo '<li><a href="#profile"><span class="glyphicon glyphicon-user"></span></a></li>';
                                        echo '<li><a href="#profile"><img src="img/user.png" width="40" height="40" style=""></a></li>';
                                    }
                                ?>
                            </ul>
                        </div>
                    </div>
                <!-- </div> -->
            </header>
        <div class="main container send_order_div">
            <p><img src="img/success.png" width="120" height="100" alt="lorem"></p>
            <p><label class="caption">Ваша заявка успешно отправлена</label></p>
        </div>
    </div>
    </body>
</html>
<script src="js/script.js"></script>
<script>
    $(document).ready(function () {
    });
</script>