<?php
	class db{
		private $db_host='localhost';

		// private $db_user='id12672989_root';
		// private $db_pass='123456';
		// private $db_name='id12672989_localhosting';

		private $db_user='root';
		private $db_pass='';
		private $db_name='vakancii';

		public function ConnectionDB(){
			$mysqli=new mysqli($this->db_host,$this->db_user,$this->db_pass,$this->db_name);
			$mysqli->query('set names utf8');
			$mysqli->query("SET CHARSET SET utf8");
			return $mysqli;
		}
		// public function ConnectionDB(){
		// 	require_once "settings.php";
		// 	$mysqli=new mysqli($DB_HOST,$DB_USER,$DB_PASS,$DB_NAME);
		// 	$mysqli->query('set names utf8');
		// 	$mysqli->query("SET CHARSET SET utf8");
		// 	return $mysqli;
		// }
		public function insertDataBase($string){
			$mysqli=$this->ConnectionDB();
			if ($mysqli->query($string)){
                $id = $mysqli->insert_id;
                $mysqli->close();
                return $id;
			}
			$mysqli->close();
            return 0;
		}
        public function updateDataBase($string){
            $mysqli=$this->ConnectionDB();
            if ($mysqli->query($string)){
                $mysqli->close();
                return true;
            }
            $mysqli->close();
            return false;
        }
        public function getData($query){
			$mysqli=$this->ConnectionDB();
			$result=$mysqli->query($query);
			$arr=array();
			while($row=$result->fetch_assoc()){
				$arr[] = $row;
			}
		 	$result->close();
		 	$mysqli->close();
			// var_dump($arr);exit;
			return $result;			
		}
		public function getDataFetch($query){
			$mysqli=$this->ConnectionDB();
			$result=$mysqli->query($query);
			$arr=$result->fetch_assoc();
            $mysqli->close();
            // var_dump($arr);exit;
			return $arr;			
		}
		public function getDataArr($query){
			$mysqli=$this->ConnectionDB();		
			$result = array();
			$i=1;
			if(!$stmt = $mysqli->query($query)){
				echo '{"success":false,"message":"' . $mysqli->errno . ' ' . $mysqli->error . '"}';
			} else {
				for($result; $tmp = $stmt->fetch_array(MYSQLI_ASSOC);){
					$result[] = $tmp;
				}
				$stmt->close();
			}	
			return $result;
		}
		public function writeToLogFile($msg,$filename,$dir){
		  	$msg='-->'.date('Y-m-d H:i:s').' '.$msg;
			$today = date("Y_m_d");
			$logfile = $today."_".$filename."_log.txt";
			$saveLocation=$dir . '/' . $logfile;
			if(!$handle = @fopen($saveLocation, "a")){
				exit;
			}else{
				if(@fwrite($handle,"$msg\r\n") === FALSE){
					exit;
				}
				@fclose($handle);
			}
		}
	}
	
		
?>