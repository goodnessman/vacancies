<?php
require_once('db.php');
require_once('global_variable.php');
// global $DB_USER;
// global $DB_PASS;
// global $DB_HOST;
// global $DB_NAME;
// global $URL_RECEPTION_EMAIL;

class User {
    protected $db = null;
    public $id;
    public $login;
    public $role;
    public $name;
    public $surname;
    public $patronymic;
    public $email;
    public $country;
    public $checkbox13;
    public $company;
    public $phone;
    public $resume_file;
    public $resume_file_origin;
    public $testing_ans;
    protected $message;

    function __construct() {
        session_start();
    }

    function __destruct() {
        session_write_close();
    }

    public function LoginUser($login, $pass) {
        if($this->db == null) $this->db = new db();

        $md5_pass = md5($pass);

        try {
            $query = "SELECT `id`,`email`,`name`,`surname`,`patronymic`,`email`,`role`,`testing_ans`
                ,IF(`resume_file` IS NULL, 'Выберите файл...', `resume_file_origin`) AS `resume_file`
                ,IF(`resume_file_origin` IS NULL, 'Выберите файл...', `resume_file_origin`) AS `resume_file_origin`
            FROM `users` WHERE `login` = '$login' AND `password` = '$md5_pass';";
        
            $data = $this->db->getDataFetch($query);

            // var_dump($data);

            if (isset($data['id'])) {
                $this->id = $data['id'];
                $this->login = $login;
                $this->role = intval($data['role']);
                $this->name = $data['name'];
                $this->surname = $data['surname'];
                $this->patronymic = $data['patronymic'];
                $this->email = $data['email'];
                $this->resume_file = $data['resume_file'];
                $this->resume_file_origin = $data['resume_file_origin'];
                $this->testing_ans = $data['testing_ans'];
                $this->Serialize();
                $this->message = 'Авторизация успешна';
                //Udate last date
                $queryLD = "UPDATE `users` SET `date_login`=NOW() WHERE `id`='$this->id';";
                $this->db->updateDataBase($queryLD);

                return true;
            } else {
                $this->message = 'Неверные данные авторизации!';
            }
        }catch(Exception $e){
            $this->message = 'Ошибка '.$e->getMessage().' при авторизации, попробуйте снова!';
        }
        return false;
    }

    public function LogoutUser() {
        $this->id = null;
        $this->login = null;
        $this->role = null;
        $this->first_name = null;
        $this->last_name = null;
        $this->email = null;
        $this->country = null;
        $this->checkbox13 = null;
        $this->company = null;
        $this->phone = null;
        $this->resume_file = null;
        $this->resume_file_origin = null;
        $this->testing_ans = null;
        session_unset();

        $this->message = 'Logout completed successfully';
    }

    public function RestoreUser() {
        if(isset($_SESSION['id']) && isset($_SESSION['login'])) {
            $this->id = $_SESSION['id'];
            $this->login = $_SESSION['login'];
            $this->role = (isset($_SESSION['role'])) ? $_SESSION['role'] : null;
            $this->first_name = (isset($_SESSION['first_name'])) ? $_SESSION['first_name'] : null;
            $this->last_name = (isset($_SESSION['last_name'])) ? $_SESSION['last_name'] : null;
            $this->email = (isset($_SESSION['email'])) ? $_SESSION['email'] : null;
            $this->country = (isset($_SESSION['country'])) ? $_SESSION['country'] : null;
            $this->checkbox13 = (isset($_SESSION['checkbox13'])) ? $_SESSION['checkbox13'] : null;
            $this->company = (isset($_SESSION['company'])) ? $_SESSION['company'] : null;
            $this->phone = (isset($_SESSION['phone'])) ? $_SESSION['phone'] : null;
            $this->resume_file = (isset($_SESSION['resume_file'])) ? $_SESSION['resume_file'] : null;
            $this->resume_file_origin = (isset($_SESSION['resume_file_origin'])) ? $_SESSION['resume_file_origin'] : null;
            $this->testing_ans = (isset($_SESSION['testing_ans'])) ? $_SESSION['testing_ans'] : null;
            return true;
        }
        return false;
    }

    public function RegistrationUser($pass) {
        if($this->db == null) $this->db = new db();

        try{
            $query = "SELECT COUNT(*) AS `count` FROM `users` WHERE `login` = '$this->login';";
            $count = $this->db->getData($query)['count'];
            if($count > 0) {
                $this->message = 'Error registering, such account exists';
                return false;
            }

            $md5_pass = md5($pass);
            $this->login = trim($this->login);
            $this->email = trim($this->email);

            $query = null;
            switch($this->role) {
                case 1:
                    $query = "INSERT INTO `users` (`login`,`password`,`md5_pass`,`email`,`first_name`,`last_name`,`country`,`checkbox13`,`is_driver`,`role_id`,`create_date`,`last_date`) VALUES('$this->login','$pass','$md5_pass','$this->email','$this->first_name','$this->last_name','$this->country','$this->checkbox13',1,$this->role_id,sysdate(),sysdate());";
                    break;
                case 2:
                    $query = "INSERT INTO `users` (`login`,`password`,`md5_pass`,`email`,`country`,`checkbox13`,`is_driver`,`company`,`phone`,`role_id`,`create_date`,`last_date`) VALUES('$this->login','$pass','$md5_pass','$this->email','$this->country','$this->checkbox13', 0, '$this->company', '$this->phone', $this->role_id,sysdate(),sysdate());";
                    break;
                case 3:
                    $query = "INSERT INTO `users` (`login`,`password`,`md5_pass`,`email`,`first_name`,`last_name`,`country`,`checkbox13`,`is_driver`,`role_id`,`create_date`,`last_date`) VALUES('$this->login','$pass','$md5_pass','$this->email','$this->first_name','$this->last_name', '$this->country', '$this->checkbox13',1,$this->role_id,sysdate(),sysdate());";
                    break;
                case 4:
                    $query = "INSERT INTO `users` (`login`,`password`,`md5_pass`,`email`,`country`,`checkbox13`,`is_driver`,`company`,`phone`,`role_id`,`create_date`,`last_date`) VALUES('$this->login','$pass','$md5_pass','$this->email','$this->country','$this->checkbox13', 0, '$this->company', '$this->phone', $this->role_id,sysdate(),sysdate());";
                    break;
            }

            if(isset($query)) {
                $id = $this->db->insertDataBase($query);
                if($id > 0){
                    $this->id = $id;
                    $this->Serialize();

                    $this->message = 'Registration completed successfully';
                    return true;
                } else {
                    $this->message = 'Error registering, try again later';
                }
            } else {
                $this->message = 'Error registering, incorrect role';
            }
        }catch(Exception $e){
            $this->message = 'Error '.$e->getMessage().' in registering, try again later';
        }
        return false;
    }

    public function UpdateUser() {
        if($this->db == null) $this->db = new db();

        try {
            if (isset($this->id)) {
                $query = "UPDATE `users` SET `first_name`='$this->first_name',`last_name`='$this->last_name',`phone`='$this->phone' WHERE `id`='$this->id';";
                if($this->db->updateDataBase($query)) {
                    $this->Serialize();

                    $this->message = 'Save profile completed successfully';
                    return true;
                } else {
                    $this->message = 'Error save profile, try again later';
                }
            } else {
                $this->message = 'Error save profile, account not found';
            }
        }catch(Exception $e){
            $this->message = 'Error '.$e->getMessage().' in registering, try again later';
        }
        return false;
    }

    public function IsActive() {
        return isset($this->id);
    }

    public function GetID() {
        return $this->id;
    }

    public function GetLogin() {
        return $this->login;
    }

    public function GetFIO() {
        $fio = '';
        if(isset($_SESSION['id']) && isset($_SESSION['login'])) {
            $fio = $_SESSION['surname'].' '.$_SESSION['name'];
        }
        // $fio = $this->last_name.' '.$this->first_name;
        return $fio;
    }

    public function GetMessage() {
        return $this->message;
    }

    public function Serialize() {
        $_SESSION['id'] = $this->id;
        $_SESSION['login'] = $this->login;
        $_SESSION['role'] = $this->role;
        $_SESSION['name'] = $this->name;
        $_SESSION['surname'] = $this->surname;
        $_SESSION['patronymic'] = $this->patronymic;
        $_SESSION['email'] = $this->email;
        $_SESSION['country'] = $this->country;
        $_SESSION['checkbox13'] = $this->checkbox13;
        $_SESSION['company'] = $this->company;
        $_SESSION['phone'] = $this->phone;
        $_SESSION['resume_file'] = $this->resume_file;
        $_SESSION['resume_file_origin'] = $this->resume_file_origin;
        $_SESSION['testing_ans'] = $this->testing_ans;
    }

    public function getJobs(){
        $str_list_jobs = '';
        global $URL_RECEPTION_EMAIL;

        // echo 'URL_RECEPTION_EMAIL'.$URL_RECEPTION_EMAIL; exit;

        if($this->db == null) $this->db = new db();
        try {

            $query = "SELECT j.`id`,j.`head`,j.`description`, MD5(j.`id`) AS `url`
                ,IF(f.`origin_file_name` IS NULL, 'Выберите файл...', f.`origin_file_name`) AS `origin_file_name`,j.`email`,j.`company`,j.`redirect_url`
                FROM `jobs` j
                LEFT JOIN `import_files` f ON j.`file` = f.`id`
                WHERE j.`delete` = 0;";
        
            $data = $this->db->getDataArr($query);

            foreach ($data as $key => $value) {
                $str_list_jobs .= '<p class="urljobs">
                        <a data-toggle="collapse" href="#jobs_'.$value['id'].'" role="button" aria-expanded="false" aria-controls="collapseExample">'.$value['head'].'</a>
                    </p>
                    <div class="collapse" id="jobs_'.$value['id'].'">
                      <div class="card card-body">
                        <form id="jobform_'.$value['id'].'" class="jobform">
                            <div class="form-group">
                                <input class="form-control idjob" type="text" name="idjob" value="'.$value['id'].'" style="display: none;">
                                <label for="input">Вакансия</label>
                                <input class="form-control" type="text" name="jobhead" placeholder="Вакансия" value="'.$value['head'].'">
                                <label for="input">Компания</label>
                                <input class="form-control" type="text" name="jobcompany" placeholder="Компания" value="'.$value['company'].'">
                                <label for="textarea">Описание вакансии</label>
                                <textarea class="form-control" name="jobcoment" rows="3">'.$value['description'].'</textarea>
                                <div class="form-group">
                                    <label for="jobfile">Файл</label>
                                    <label class="custom-file jobfile" style="width: 100%;">
                                        <input type="file" class="form-control custom-file-input jobfileinput" name="file'.$value['id'].'" id="file'.$value['id'].'">
                                        <span class="custom-file-control" data-content-value="'.$value['origin_file_name'].'"></span>
                                    </label>
                                </div>
                                <label for="input">Ссылка</label>
                                <input class="form-control" type="text" value="'.$URL_RECEPTION_EMAIL.$value['url'].'">
                                <label for="input">Email для получения заявок</label>
                                <input class="form-control" type="email" name="email" placeholder="Email" value="'.$value['email'].'">
                                <label for="input">Переадресация</label>
                                <input class="form-control" type="text" name="redirect_url" placeholder="https://..." value="'.$value['redirect_url'].'">
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-primary savejob">Сохранить</button>
                                    <button type="submit" class="btn btn-primary deljob">Удалить</button>
                                </div>
                            </div>
                        </form>
                      </div>
                    </div>';
            }

            return $str_list_jobs;
        }catch(Exception $e){
            return $this->message = 'Ошибка '.$e->getMessage().', попробуйте снова!';
        }
        return $str_list_jobs;
    }

    public function getJobsByURL($url){
        if($this->db == null) $this->db = new db();
        $data = array();
        try {
            $query = "SELECT j.`id`,j.`head`,j.`description`,j.`file`,j.`email`,MAX(f.`id`) AS `file_id`,j.`redirect_url`,j.`company`
                FROM `jobs` j
                LEFT JOIN `import_files` f ON j.`id` = f.`id_job`
                WHERE md5(j.`id`)='$url' LIMIT 1;";
            $data = $this->db->getDataFetch($query);
            // return $data;
            if (!isset($data['id'])) {
                $data = array(
                    'head'=>'Нет данных по вакансии!',
                    'description'=>'Нет данных по вакансии!',
                    'file_id'=>null,
                );
            }
            if ( $data['file_id'] != null ) {
                $query = "SELECT f.`file_name`,f.`origin_file_name`,f.`id` FROM `import_files` f WHERE f.`id` = ".$data['file_id'].";";
                $data2 = $this->db->getDataFetch($query);
                if (isset($data2['id'])) {
                    // $data['query'] = $query;
                    $data['file_id'] = $data2['id'];
                    $data['file_name'] = $data2['file_name'];
                    $data['origin_file_name'] = $data2['origin_file_name'];
                }
            }
            return $data;
        }catch(Exception $e){
            return $this->message = 'Ошибка '.$e->getMessage().', попробуйте снова!';
        }
        return $data;
    }

    public function GetUserMotivation($id_user,$vakanciya_id) {
        if($this->db == null) $this->db = new db();
        $motivation = '';
        $this->RestoreUser();
        try{
            $query = "SELECT `motivation` FROM `motivation` WHERE `user_id` = $id_user AND `jobs_id` = $vakanciya_id;";
            $data = $this->db->getDataArr($query);
            $motivation = $data[0]['motivation'];
            // var_dump($data); exit;
        }catch(Exception $e){
            return $this->message = 'Ошибка '.$e->getMessage().', попробуйте снова!';
        }
        return $motivation;
    }

    public function getUserAchievementsModal($id_user, $vakanciya_id) {
        if($this->db == null) $this->db = new db();
        try{
            $query = "SELECT `id`,`role`,`year_start`,`year_end`,`month_start`,`month_end`,`tasks`,`result`,`start_date`,`end_date`
                FROM `achievements` 
                WHERE `user_id` = $id_user AND `jobs_id` = $vakanciya_id;";
            $data = $this->db->getDataArr($query);
            $block = '';
            /*$arr_month = array('Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь');
            $arr_year = array('2020','2021','2022','2023','2024','2025','2026','2027','2028','2029','2030');*/

            /*if (isset($data[0]['id'])) {
                foreach ($data as $key => $value) {
                    $mr_t = '';
                    if ($key!=0) {
                        $mr_t = 'margin-top: 25px;';
                    }

                    $block .= '<div class="col-lg-12 achievements_block achievements_item" style="'.$mr_t.'">'.
                        '<div class="col-lg-4 achievements_block date">'.
                            '<label class="" for="inputEmail">Общая информация</label>'.
                            '<input type="role" name="role" class="form-control role" placeholder="Твоя роль" autofocus="true" value="'.$value['role'].'">'.

                            '<div class="col-lg-6 achievements_block">';
                                // '<input type="start_month" name="start_month" class="form-control start_month" placeholder="Месяц" value="'.$value['month_start'].'">'.
                                $class = '';
                                if (!$value['month_start']) { $class = 'color_place_holder'; }
                                $block .='<select type="text" class="dop_info_select form-control start_month '.$class.'" name="start_month" value="'.$value['month_start'].'"><option value="Месяц" class="white_color">Месяц</option>';
                                    foreach ($arr_month as $key2 => $val) {
                                        $selected = '';
                                        if ($val==$value['month_start']) {
                                            $selected = 'selected';
                                        }
                                        $block .='<option value="'.$val.'" class="black_color" '.$selected.'>'.$val.'</option>';
                                    }
                                $block .='</select>';
                                $class = '';
                                if (!$value['month_end']) { $class = 'color_place_holder'; }
                                 $block .='<select type="text" class="dop_info_select form-control end_month '.$class.'" name="end_month" value="'.$value['end_month'].'"><option value="Месяц" class="white_color">Месяц</option>';
                                    foreach ($arr_month as $key2 => $val) {
                                        $selected = '';
                                        if ($val==$value['month_end']) {
                                            $selected = 'selected';
                                        }
                                        $block .='<option value="'.$val.'" class="black_color" '.$selected.'>'.$val.'</option>';
                                    }
                                $block .='</select>';
                                // $block .='<input type="end_month" name="end_month" class="form-control end_month" placeholder="Месяц" value="'.$value['month_end'].'">';
                            $block .='</div>';
                            $block .='<div class="col-lg-6 achievements_block">';

                                $class = '';
                                if (!$value['year_start']) { $class = 'color_place_holder'; }
                                 $block .='<select type="text" class="dop_info_select form-control start_year '.$class.'" name="start_year" value="'.$value['year_start'].'"><option value="Год начала" class="white_color">Год начала</option>';
                                    foreach ($arr_year as $key2 => $val) {
                                        $selected = '';
                                        if ($val==$value['year_start']) {
                                            $selected = 'selected';
                                        }
                                        $block .='<option value="'.$val.'" class="black_color" '.$selected.'>'.$val.'</option>';
                                    }
                                $block .='</select>';
                                $class = '';
                                if (!$value['year_end']) { $class = 'color_place_holder'; }
                                 $block .='<select type="text" class="dop_info_select form-control end_year '.$class.'" name="end_year" value="'.$value['year_end'].'"><option value="Год окончания" class="white_color">Год окончания</option>';
                                    foreach ($arr_year as $key2 => $val) {
                                        $selected = '';
                                        if ($val==$value['year_end']) {
                                            $selected = 'selected';
                                        }
                                        $block .='<option value="'.$val.'" class="black_color" '.$selected.'>'.$val.'</option>';
                                    }
                                $block .='</select>';

                                // $block .='<input type="start_year" name="start_year" class="form-control start_year" placeholder="Год начала" value="'.$value['year_start'].'">'.
                                // '<input type="end_year" name="end_year" class="form-control end_year" placeholder="Год окончания" value="'.$value['year_end'].'">';

                            $block .='</div>'.
                        '</div>'.
                        '<div class="col-lg-4" style="padding-left: 30px;">'.
                            '<label class="">Твои задачи</label>'.
                            '<textarea class="form-control task" rows="6" spellcheck="false" data-gramm="false" placeholder="Введите текст">'.$value['tasks'].'</textarea>'.
                        '</div>'.
                        '<div class="col-lg-4 ">'.
                            '<label class="">Результаты</label>'.
                            '<textarea class="form-control results" rows="5" spellcheck="false" data-gramm="false" placeholder="Введите текст">'.$value['result'].'</textarea>'.
                        '</div>'.
                    '</div>';
                }
            }
            /*else{
                $block = '<div class="col-lg-12 achievements_block achievements_item">'.
                        '<div class="col-lg-4 achievements_block date">'.
                            '<label class="" for="inputEmail">Общая информация</label>'.
                            '<input type="role" name="role" class="form-control role" placeholder="Твоя роль" autofocus="true">'.
                            '<div class="col-lg-6 achievements_block">
                                <select type="text" class="dop_info_select form-control start_month color_place_holder" name="start_month">
                                    <option value="Месяц" class="white_color">Месяц</option>
                                    <option value="Январь" class="black_color">Январь</option>
                                    option value="Февраль" class="black_color">Февраль</option>
                                    <option value="Март" class="black_color">Март</option>
                                    <option value="Апрель" class="black_color">Апрель</option>
                                    <option value="Май" class="black_color">Май</option>
                                    <option value="Июнь" class="black_color">Июнь</option>
                                    <option value="Июль" class="black_color">Июль</option>
                                    <option value="Август" class="black_color">Август</option>
                                    <option value="Сентябрь" class="black_color">Сентябрь</option>
                                    <option value="Октябрь" class="black_color">Октябрь</option>
                                    <option value="Ноябрь" class="black_color">Ноябрь</option>
                                    <option value="Декабрь" class="black_color">Декабрь</option>
                                </select>
                                <select type="text" class="dop_info_select form-control end_month color_place_holder" name="end_month">
                                    <option value="Месяц" class="white_color">Месяц</option>
                                    <option value="Январь" class="black_color">Январь</option>
                                    <option value="Февраль" class="black_color">Февраль</option>
                                    <option value="Март" class="black_color">Март</option>
                                    <option value="Апрель" class="black_color">Апрель</option>
                                    <option value="Май" class="black_color">Май</option>
                                    <option value="Июнь" class="black_color">Июнь</option>
                                    <option value="Июль" class="black_color">Июль</option>
                                    <option value="Август" class="black_color">Август</option>
                                    <option value="Сентябрь" class="black_color">Сентябрь</option>
                                    <option value="Октябрь" class="black_color">Октябрь</option>
                                    <option value="Ноябрь" class="black_color">Ноябрь</option>
                                    <option value="Декабрь" class="black_color">Декабрь</option>
                                </select>
                            </div>
                            <div class="col-lg-6 achievements_block">
                                <select class="dop_info_select form-control start_year color_place_holder" name="start_year">
                                    <option value="Год начала" class="white_color">Год начала</option>
                                    <option value="2020" class="black_color">2020</option>
                                    <option value="2021" class="black_color">2021</option>
                                    <option value="2022" class="black_color">2022</option>
                                    <option value="2023" class="black_color">2023</option>
                                    <option value="2024" class="black_color">2024</option>
                                    <option value="2025" class="black_color">2025</option>
                                    <option value="2026" class="black_color">2026</option>
                                    <option value="2027" class="black_color">2027</option>
                                    <option value="2028" class="black_color">2028</option>
                                    <option value="2029" class="black_color">2029</option>
                                    <option value="2030" class="black_color">2030</option>
                                </select>
                                <select class="dop_info_select form-control end_year color_place_holder" name="end_year">
                                    <option value="Год окончания" class="white_color">Год окончания</option>
                                    <option value="2020" class="black_color">2020</option>
                                    <option value="2021" class="black_color">2021</option>
                                    <option value="2022" class="black_color">2022</option>
                                    <option value="2023" class="black_color">2023</option>
                                    <option value="2024" class="black_color">2024</option>
                                    <option value="2025" class="black_color">2025</option>
                                    <option value="2026" class="black_color">2026</option>
                                    <option value="2027" class="black_color">2027</option>
                                    <option value="2028" class="black_color">2028</option>
                                    <option value="2029" class="black_color">2029</option>
                                    <option value="2030" class="black_color">2030</option>
                                </select>
                            </div>'.
                            // '<div class="col-lg-6 achievements_block">'.
                            //     '<input type="start_year" name="start_year" class="form-control start_year" placeholder="Год начала">'.
                            //     '<input type="end_year" name="end_year" class="form-control end_year" placeholder="Год окончания">'.
                            // '</div>'.
                            // '<div class="col-lg-6 achievements_block">'.
                            //     '<input type="start_month" name="start_month" class="form-control start_month" placeholder="Месяц">'.
                            //     '<input type="end_month" name="end_month" class="form-control end_month" placeholder="Месяц">'.
                            // '</div>'.
                        '</div>'.
                        '<div class="col-lg-4" style="padding-left: 30px;">'.
                            '<label class="">Твои задачи</label>'.
                            '<textarea class="form-control task" rows="6" spellcheck="false" data-gramm="false" placeholder="Введите текст"></textarea>'.
                        '</div>'.
                        '<div class="col-lg-4">'.
                            '<label class="">Результаты</label>'.
                            '<textarea class="form-control results" rows="5" spellcheck="false" data-gramm="false" placeholder="Введите текст"></textarea>'.
                        '</div>'.
                    '</div>';
            }*/
            // var_dump($data); exit;
        }catch(Exception $e){
            return $this->message = 'Ошибка '.$e->getMessage().', попробуйте снова!';
        }
        $res = array('data' => $data, 'count' => count($data) );
        return $res;
    }

    public function GetUserAchievements($id_user, $vakanciya_id) {
        if($this->db == null) $this->db = new db();
        try{
            $query = "SELECT `id`,`role`,`year_start`,`year_end`,`month_start`,`month_end`
                ,`tasks`,`result` FROM `achievements` WHERE `user_id` = $id_user AND `jobs_id` = $vakanciya_id;";
            $data = $this->db->getDataArr($query);
            $block = '';
            $arr_month = array('Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь');
            $arr_year = array('2020','2021','2022','2023','2024','2025','2026','2027','2028','2029','2030');

            if (isset($data[0]['id'])) {
                foreach ($data as $key => $value) {
                    $mr_t = '';
                    if ($key!=0) {
                        $mr_t = 'margin-top: 25px;';
                    }

                    $block .= '<div class="col-lg-12 achievements_block achievements_item" style="'.$mr_t.'">'.
                        '<div class="col-lg-4 achievements_block date">'.
                            '<label class="" for="inputEmail">Общая информация</label>'.
                            '<input type="role" name="role" class="form-control role" placeholder="Твоя роль" autofocus="true" value="'.$value['role'].'">'.

                            '<div class="col-lg-6 achievements_block">';
                                // '<input type="start_month" name="start_month" class="form-control start_month" placeholder="Месяц" value="'.$value['month_start'].'">'.
                                $class = '';
                                if (!$value['month_start']) { $class = 'color_place_holder'; }
                                $block .='<select type="text" class="dop_info_select form-control start_month '.$class.'" name="start_month" value="'.$value['month_start'].'"><option value="Месяц" class="white_color">Месяц</option>';
                                    foreach ($arr_month as $key2 => $val) {
                                        $selected = '';
                                        if ($val==$value['month_start']) {
                                            $selected = 'selected';
                                        }
                                        $block .='<option value="'.$val.'" class="black_color" '.$selected.'>'.$val.'</option>';
                                    }
                                $block .='</select>';
                                $class = '';
                                if (!$value['month_end']) { $class = 'color_place_holder'; }
                                 $block .='<select type="text" class="dop_info_select form-control end_month '.$class.'" name="end_month" value="'.$value['end_month'].'"><option value="Месяц" class="white_color">Месяц</option>';
                                    foreach ($arr_month as $key2 => $val) {
                                        $selected = '';
                                        if ($val==$value['month_end']) {
                                            $selected = 'selected';
                                        }
                                        $block .='<option value="'.$val.'" class="black_color" '.$selected.'>'.$val.'</option>';
                                    }
                                $block .='</select>';
                                // $block .='<input type="end_month" name="end_month" class="form-control end_month" placeholder="Месяц" value="'.$value['month_end'].'">';
                            $block .='</div>';
                            $block .='<div class="col-lg-6 achievements_block">';

                                $class = '';
                                if (!$value['year_start']) { $class = 'color_place_holder'; }
                                 $block .='<select type="text" class="dop_info_select form-control start_year '.$class.'" name="start_year" value="'.$value['year_start'].'"><option value="Год начала" class="white_color">Год начала</option>';
                                    foreach ($arr_year as $key2 => $val) {
                                        $selected = '';
                                        if ($val==$value['year_start']) {
                                            $selected = 'selected';
                                        }
                                        $block .='<option value="'.$val.'" class="black_color" '.$selected.'>'.$val.'</option>';
                                    }
                                $block .='</select>';
                                $class = '';
                                if (!$value['year_end']) { $class = 'color_place_holder'; }
                                 $block .='<select type="text" class="dop_info_select form-control end_year '.$class.'" name="end_year" value="'.$value['year_end'].'"><option value="Год окончания" class="white_color">Год окончания</option>';
                                    foreach ($arr_year as $key2 => $val) {
                                        $selected = '';
                                        if ($val==$value['year_end']) {
                                            $selected = 'selected';
                                        }
                                        $block .='<option value="'.$val.'" class="black_color" '.$selected.'>'.$val.'</option>';
                                    }
                                $block .='</select>';

                                // $block .='<input type="start_year" name="start_year" class="form-control start_year" placeholder="Год начала" value="'.$value['year_start'].'">'.
                                // '<input type="end_year" name="end_year" class="form-control end_year" placeholder="Год окончания" value="'.$value['year_end'].'">';

                            $block .='</div>'.
                        '</div>'.
                        '<div class="col-lg-4" style="padding-left: 30px;">'.
                            '<label class="">Твои задачи</label>'.
                            '<textarea class="form-control task" rows="6" spellcheck="false" data-gramm="false" placeholder="Введите текст">'.$value['tasks'].'</textarea>'.
                        '</div>'.
                        '<div class="col-lg-4 ">'.
                            '<label class="">Результаты</label>'.
                            '<textarea class="form-control results" rows="5" spellcheck="false" data-gramm="false" placeholder="Введите текст">'.$value['result'].'</textarea>'.
                        '</div>'.
                    '</div>';
                }
            }else{
                $block = '<div class="col-lg-12 achievements_block achievements_item">'.
                        '<div class="col-lg-4 achievements_block date">'.
                            '<label class="" for="inputEmail">Общая информация</label>'.
                            '<input type="role" name="role" class="form-control role" placeholder="Твоя роль" autofocus="true">'.
                            '<div class="col-lg-6 achievements_block">
                                <select type="text" class="dop_info_select form-control start_month color_place_holder" name="start_month">
                                    <option value="Месяц" class="white_color">Месяц</option>
                                    <option value="Январь" class="black_color">Январь</option>
                                    option value="Февраль" class="black_color">Февраль</option>
                                    <option value="Март" class="black_color">Март</option>
                                    <option value="Апрель" class="black_color">Апрель</option>
                                    <option value="Май" class="black_color">Май</option>
                                    <option value="Июнь" class="black_color">Июнь</option>
                                    <option value="Июль" class="black_color">Июль</option>
                                    <option value="Август" class="black_color">Август</option>
                                    <option value="Сентябрь" class="black_color">Сентябрь</option>
                                    <option value="Октябрь" class="black_color">Октябрь</option>
                                    <option value="Ноябрь" class="black_color">Ноябрь</option>
                                    <option value="Декабрь" class="black_color">Декабрь</option>
                                </select>
                                <select type="text" class="dop_info_select form-control end_month color_place_holder" name="end_month">
                                    <option value="Месяц" class="white_color">Месяц</option>
                                    <option value="Январь" class="black_color">Январь</option>
                                    <option value="Февраль" class="black_color">Февраль</option>
                                    <option value="Март" class="black_color">Март</option>
                                    <option value="Апрель" class="black_color">Апрель</option>
                                    <option value="Май" class="black_color">Май</option>
                                    <option value="Июнь" class="black_color">Июнь</option>
                                    <option value="Июль" class="black_color">Июль</option>
                                    <option value="Август" class="black_color">Август</option>
                                    <option value="Сентябрь" class="black_color">Сентябрь</option>
                                    <option value="Октябрь" class="black_color">Октябрь</option>
                                    <option value="Ноябрь" class="black_color">Ноябрь</option>
                                    <option value="Декабрь" class="black_color">Декабрь</option>
                                </select>
                            </div>
                            <div class="col-lg-6 achievements_block">
                                <select class="dop_info_select form-control start_year color_place_holder" name="start_year">
                                    <option value="Год начала" class="white_color">Год начала</option>
                                    <option value="2020" class="black_color">2020</option>
                                    <option value="2021" class="black_color">2021</option>
                                    <option value="2022" class="black_color">2022</option>
                                    <option value="2023" class="black_color">2023</option>
                                    <option value="2024" class="black_color">2024</option>
                                    <option value="2025" class="black_color">2025</option>
                                    <option value="2026" class="black_color">2026</option>
                                    <option value="2027" class="black_color">2027</option>
                                    <option value="2028" class="black_color">2028</option>
                                    <option value="2029" class="black_color">2029</option>
                                    <option value="2030" class="black_color">2030</option>
                                </select>
                                <select class="dop_info_select form-control end_year color_place_holder" name="end_year">
                                    <option value="Год окончания" class="white_color">Год окончания</option>
                                    <option value="2020" class="black_color">2020</option>
                                    <option value="2021" class="black_color">2021</option>
                                    <option value="2022" class="black_color">2022</option>
                                    <option value="2023" class="black_color">2023</option>
                                    <option value="2024" class="black_color">2024</option>
                                    <option value="2025" class="black_color">2025</option>
                                    <option value="2026" class="black_color">2026</option>
                                    <option value="2027" class="black_color">2027</option>
                                    <option value="2028" class="black_color">2028</option>
                                    <option value="2029" class="black_color">2029</option>
                                    <option value="2030" class="black_color">2030</option>
                                </select>
                            </div>'.
                            // '<div class="col-lg-6 achievements_block">'.
                            //     '<input type="start_year" name="start_year" class="form-control start_year" placeholder="Год начала">'.
                            //     '<input type="end_year" name="end_year" class="form-control end_year" placeholder="Год окончания">'.
                            // '</div>'.
                            // '<div class="col-lg-6 achievements_block">'.
                            //     '<input type="start_month" name="start_month" class="form-control start_month" placeholder="Месяц">'.
                            //     '<input type="end_month" name="end_month" class="form-control end_month" placeholder="Месяц">'.
                            // '</div>'.
                        '</div>'.
                        '<div class="col-lg-4" style="padding-left: 30px;">'.
                            '<label class="">Твои задачи</label>'.
                            '<textarea class="form-control task" rows="6" spellcheck="false" data-gramm="false" placeholder="Введите текст"></textarea>'.
                        '</div>'.
                        '<div class="col-lg-4">'.
                            '<label class="">Результаты</label>'.
                            '<textarea class="form-control results" rows="5" spellcheck="false" data-gramm="false" placeholder="Введите текст"></textarea>'.
                        '</div>'.
                    '</div>';
            }
            // var_dump($data); exit;
        }catch(Exception $e){
            return $this->message = 'Ошибка '.$e->getMessage().', попробуйте снова!';
        }
        $res = array('block' => $block, 'count' => count($data) );
        return $res;
    }

    public function getCountAchievements($id_user,$vakanciya_id){
        if($this->db == null) $this->db = new db();
        try{
            if (!isset($id_user)){return 0;}
            $query = "SELECT COUNT(`id`) AS `count` FROM `achievements` WHERE `user_id` = $id_user AND `jobs_id` = $vakanciya_id;";
            $data = $this->db->getDataArr($query);
            $count = $data[0]['count'];
        }catch(Exception $e){
            return $this->message = 'Ошибка '.$e->getMessage().', попробуйте снова!';
        }
        return $count;
    }

    public function GetUserAchievementsForMail($id_user, $vakanciya_id) {
        if($this->db == null) $this->db = new db();
        try{
            $query = "SELECT `id`,`role`,`year_start`,`year_end`,`month_start`,`month_end`
                ,`tasks`,`result` FROM `achievements` WHERE `user_id` = $id_user AND `jobs_id` = $vakanciya_id;";
            $data = $this->db->getDataArr($query);
            $block = '';
            foreach ($data as $key => $value) {

                $block .= '
                    <tr style="border-collapse:collapse;"> 
                      <td align="left" bgcolor="#ffffff" style="padding:0;Margin:0;padding-top:20px;padding-left:30px;padding-right:30px;background-color:#FFFFFF;">
                       
                       <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td width="260" class="es-m-p20b" align="left" style="padding:0;Margin:0;"> 
                           <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:separate;border-spacing:0px;border-left:1px solid #D4D4D4;border-right:1px solid #D4D4D4;border-top:1px solid #D4D4D4;border-bottom:1px solid #D4D4D4;border-top-left-radius:7px;border-top-right-radius:7px;border-bottom-right-radius:7px;border-bottom-left-radius:7px;" role="presentation"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="center" style="padding:0;Margin:0;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:18px;font-family:roboto, \'helvetica neue\', helvetica, arial, sans-serif;line-height:27px;color:#000000;">'.$value['role'].'</p></td> 
                             </tr> 
                           </table></td> 
                         </tr> 
                       </table>

                     </td> 
                     </tr> 

                     <tr style="border-collapse:collapse;"> 
                      <td align="left" class="my" bgcolor="#ffffff" style="padding:0;Margin:0;padding-top:20px;padding-left:30px;padding-right:30px;background-color:#FFFFFF;">
                       
                       <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td width="120" class="es-m-p0r es-m-p20b" align="center" style="padding:0;Margin:0;"> 
                           <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:separate;border-spacing:0px;border-left:1px solid #D4D4D4;border-right:1px solid #D4D4D4;border-top:1px solid #D4D4D4;border-bottom:1px solid #D4D4D4;border-top-left-radius:7px;border-top-right-radius:7px;border-bottom-right-radius:7px;border-bottom-left-radius:7px;" role="presentation"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="center" style="padding:0;Margin:0;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:19px;font-family:roboto, \'helvetica neue\', helvetica, arial, sans-serif;line-height:29px;color:#000000;">'.$value['month_start'].'</p></td> 
                             </tr> 
                           </table></td> 
                          <td class="es-hidden" width="20" style="padding:0;Margin:0;"></td> 
                         </tr> 
                       </table>

                       <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td width="120" class="es-m-p20b" align="center" style="padding:0;Margin:0;"> 
                           <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:separate;border-spacing:0px;border-left:1px solid #D4D4D4;border-right:1px solid #D4D4D4;border-top:1px solid #D4D4D4;border-bottom:1px solid #D4D4D4;border-top-left-radius:7px;border-top-right-radius:7px;border-bottom-right-radius:7px;border-bottom-left-radius:7px;" role="presentation"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="center" style="padding:0;Margin:0;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:18px;font-family:roboto, \'helvetica neue\', helvetica, arial, sans-serif;line-height:27px;color:#000000;">'.$value['year_start'].'</p></td> 
                             </tr> 
                           </table></td> 
                          <td class="es-hidden" width="20" style="padding:0;Margin:0;"></td> 
                         </tr> 
                       </table>
                     </td> 
                     </tr> 

                     <tr style="border-collapse:collapse;"> 
                      <td align="left" class="my" bgcolor="#ffffff" style="padding:0;Margin:0;padding-top:20px;padding-left:30px;padding-right:30px;background-color:#FFFFFF;">
                       
                       <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td width="120" class="es-m-p0r es-m-p20b" align="center" style="padding:0;Margin:0;"> 
                           <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:separate;border-spacing:0px;border-left:1px solid #D4D4D4;border-right:1px solid #D4D4D4;border-top:1px solid #D4D4D4;border-bottom:1px solid #D4D4D4;border-top-left-radius:7px;border-top-right-radius:7px;border-bottom-right-radius:7px;border-bottom-left-radius:7px;" role="presentation"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="center" style="padding:0;Margin:0;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:18px;font-family:roboto, \'helvetica neue\', helvetica, arial, sans-serif;line-height:27px;color:#000000;">'.$value['month_end'].'</p></td> 
                             </tr> 
                           </table></td> 
                          <td class="es-hidden" width="20" style="padding:0;Margin:0;"></td> 
                         </tr> 
                       </table>

                       <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td width="120" class="es-m-p20b" align="center" style="padding:0;Margin:0;"> 
                           <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:separate;border-spacing:0px;border-left:1px solid #D4D4D4;border-right:1px solid #D4D4D4;border-top:1px solid #D4D4D4;border-bottom:1px solid #D4D4D4;border-top-left-radius:7px;border-top-right-radius:7px;border-bottom-right-radius:7px;border-bottom-left-radius:7px;" role="presentation"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="center" style="padding:0;Margin:0;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:18px;font-family:roboto, \'helvetica neue\', helvetica, arial, sans-serif;line-height:27px;color:#000000;">'.$value['year_end'].'</p></td> 
                             </tr> 
                           </table></td> 
                          <td class="es-hidden" width="20" style="padding:0;Margin:0;"></td> 
                         </tr> 
                       </table> 
                     </td> 
                     </tr> 
                     
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" bgcolor="#ffffff" style="padding:0;Margin:0;padding-top:20px;padding-left:30px;padding-right:30px;background-color:#FFFFFF;"> 
                       <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td width="540" align="center" valign="top" style="padding:0;Margin:0;"> 
                           <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="left" style="padding:0;Margin:0;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, \'helvetica neue\', helvetica, arial, sans-serif;line-height:24px;color:#016FB9;">Мои задачи</p></td> 
                             </tr> 
                           </table></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" bgcolor="#ffffff" style="padding:0;Margin:0;padding-top:20px;padding-left:30px;padding-right:30px;background-color:#FFFFFF;"> 
                       <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td width="540" align="center" valign="top" style="padding:0;Margin:0;"> 
                           <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:separate;border-spacing:0px;border-left:1px solid #D4D4D4;border-right:1px solid #D4D4D4;border-top:1px solid #D4D4D4;border-bottom:1px solid #D4D4D4;border-top-left-radius:7px;border-top-right-radius:7px;border-bottom-right-radius:7px;border-bottom-left-radius:7px;" role="presentation"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="left" style="padding:10px;Margin:0;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, \'helvetica neue\', helvetica, arial, sans-serif;line-height:24px;color:#131313;">'.$value['tasks'].'</p></td> 
                             </tr> 
                           </table></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" bgcolor="#ffffff" style="padding:0;Margin:0;padding-top:20px;padding-left:30px;padding-right:30px;background-color:#FFFFFF;"> 
                       <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td width="540" align="center" valign="top" style="padding:0;Margin:0;"> 
                           <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="left" style="padding:0;Margin:0;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, \'helvetica neue\', helvetica, arial, sans-serif;line-height:24px;color:#016FB9;">Результаты</p></td> 
                             </tr> 
                           </table></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="Margin:0;padding-top:5px;padding-bottom:30px;padding-left:30px;padding-right:30px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:10px;border-bottom-left-radius:10px;background-color:#FFFFFF;"> 
                       <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td width="540" align="center" valign="top" style="padding:0;Margin:0;"> 
                           <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:separate;border-spacing:0px;border-left:1px solid #D4D4D4;border-right:1px solid #D4D4D4;border-top:1px solid #D4D4D4;border-bottom:1px solid #D4D4D4;border-top-left-radius:7px;border-top-right-radius:7px;border-bottom-right-radius:7px;border-bottom-left-radius:7px;" role="presentation"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="left" style="padding:10px;Margin:0;"> 
                               <div> 
                                <span style="text-align:center;">'.$value['result'].'</span> 
                               </div></td> 
                             </tr> 
                           </table></td> 
                         </tr> 
                       </table></td> 
                     </tr> ';
            }

        }catch(Exception $e){
            return $this->message = 'Ошибка '.$e->getMessage().', попробуйте снова!';
        }
        return $block;
    }

    public function getMotivation($id_user,$vakanciya_id){
        if($this->db == null) $this->db = new db();
        try{
            if (!isset($id_user)){return 0;}
            $query = "SELECT COUNT(`id`) AS `count` FROM `motivation` WHERE `user_id` = $id_user AND `jobs_id` = $vakanciya_id;";
            $data = $this->db->getDataArr($query);
            $count = $data[0]['count'];
        }catch(Exception $e){
            return $this->message = 'Ошибка '.$e->getMessage().', попробуйте снова!';
        }
        return $count;
    }

}
?>