<?php
// session_start();
// require_once('db.php');
// $result = new db();
// $db = $result->ConnectionDB();
ini_set('error_reporting', E_ALL);
ini_set('error_reporting', E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

if(isset($_REQUEST['action'])){
    switch($_REQUEST['action']) {
        case "getUserAchievementsModal":
            $ret = getUserAchievementsModal();
        break;
        case "getUserAchievements":
            $ret = getUserAchievements();
        break;
        case "addAchievements":
            $ret = addAchievements();
        break;
        case "getUserMotivation":
            $ret = getUserMotivation();
        break;
        case "addMotivation":
            $ret = addMotivation();
        break;
        case "sendAnswerQuestion":
            $ret = sendAnswerQuestion();
        break;
        case "loginVK":
            $ret = LoginVK();
        break;
        case "sendorder":
            $ret = SendOrder();
        break;
        case "file_upload_resume":
            $ret = UploadFileResume();
        break;
        case "registr":
            $ret = Registr();
        break;
        case "login":
            $ret = Login();
        break;
        case "logout":
            $ret = Logout();
        break;
        case "savejob":
            $ret = SaveJob();
        break;
        case "deljob":
            $ret = DeleteJob();
        break;
        case "file_upload":
            $ret = UploadFile();
        break;
        
        default:
            $ret = array();
            $ret['success'] = false;
            $ret['message'] = "Incorrect action";
        break;
    }
    echo json_encode($ret);
    exit;
}

function getUserAchievementsModal(){
    require_once('user.php');
    $user = new User();
    require_once('user.php');
    $user = new User();
    $user->RestoreUser();
    $id_user = $user->id;
    $vakanciya_id = $_REQUEST["vakanciya_id"];
    $achievements = $user->getUserAchievementsModal($id_user, $vakanciya_id);
    $resp['data'] = $achievements['data'];
    $resp['count'] = $achievements['count'];
    $resp['success'] = true;
    return $resp;
}

function getUserAchievements(){
    require_once('user.php');
    $user = new User();
    require_once('user.php');
    $user = new User();
    $user->RestoreUser();
    $id_user = $user->id;
    $vakanciya_id = $_REQUEST["vakanciya_id"];
    $achievements = $user->GetUserAchievements($id_user, $vakanciya_id);
    $resp['block'] = $achievements['block'];
    $resp['count'] = $achievements['count'];
    $resp['success'] = true;
    return $resp;
}

function addAchievements(){
    require_once('db.php');
    $result = new db();
    require_once('user.php');
    $user = new User();
    $user->RestoreUser();
    $id_user = $user->id;
    $achievements_items = $_REQUEST["achievements_items"];
    $vakanciya_id = $_REQUEST["vakanciya_id"];
    $query = "DELETE FROM `achievements` WHERE `user_id` = $id_user AND `jobs_id` = $vakanciya_id;";
    $result->updateDataBase($query);
    foreach ($achievements_items as $key => $value) {
        $resp['achievements_items_id'] = $value['id_item'];
        $query = "INSERT INTO `achievements` (`role`,`year_start`,`year_end`,`month_start`,`month_end`
                ,`tasks`,`result`,`user_id`,`jobs_id`) 
            VALUES ('".$value['role']."','".$value['start_year']."','".$value['end_year']."','".$value['start_month']."','".$value['end_month']."','".$value['task']."','".$value['results']."',$id_user,$vakanciya_id);";
        $res = $result->updateDataBase($query);
    }
    // $resp['query'] = $query;
    $resp['success'] = $res;
    return $resp;
}

function getUserMotivation(){
    require_once('user.php');
    $user = new User();
    require_once('user.php');
    $user = new User();
    $user->RestoreUser();
    $id_user = $user->id;
    $vakanciya_id = $_REQUEST["vakanciya_id"];
    $motivation = $user->GetUserMotivation($id_user,$vakanciya_id);
    $resp['motivation'] = $motivation;
    $resp['success'] = true;
    return $resp;
}

function addMotivation(){
    require_once('db.php');
    $result = new db();
    $motivation = trim($_REQUEST["motivation"]);
    $vakanciya_id = $_REQUEST["vakanciya_id"];
    require_once('user.php');
    $user = new User();
    $user->RestoreUser();
    $id_user = $user->id;
    $query = "DELETE FROM `motivation` WHERE `user_id` = $id_user AND `jobs_id` = $vakanciya_id;";
    $res = $result->updateDataBase($query);
    $query = "INSERT INTO `motivation` (`motivation`,`user_id`,`jobs_id`) VALUES ('$motivation',$id_user,$vakanciya_id)";
    // $query = "UPDATE `motivation` SET `motivation`='$motivation' WHERE `user_id` = $id_user AND `jobs_id` = $vakanciya_id;";
    $res = $result->insertDataBase($query);
    // $resp['query'] = $query;
    $resp['motivation'] = $motivation;
    $resp['success'] = true;
    return $resp;
}

function sendAnswerQuestion(){
    require_once('db.php');
    $result = new db();
    $count_success_quetion = intval($_REQUEST["count_success_quetion"]);
    require_once('user.php');
    $user = new User();
    $user->RestoreUser();
    $id_user = $user->id;
    date_default_timezone_set('America/Los_Angeles');

    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
    header('Access-Control-Allow-Methods: GET, POST, PUT');

    /** Include PHPExcel */
    require_once 'PHPExcel/Classes/PHPExcel.php';
    require_once 'PHPExcel/Classes/PHPExcel/IOFactory.php';

    $objPHPExcel = PHPExcel_IOFactory::load("../test_Excel/results.xlsx");
    $objPHPExcel->setActiveSheetIndex(0);
    $row = $objPHPExcel->getActiveSheet()->getHighestRow()+1;
    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, $user->GetFIO());
    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, '');
    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $count_success_quetion);
    $objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, '');
    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, '');
    $objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, '');
    $objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, '');
    $objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, '');
    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
    $objWriter->save('../test_Excel/results.xlsx');

    $query = "UPDATE `users` SET `testing_ans`=$count_success_quetion WHERE `id`=$id_user;";
    $res = $result->updateDataBase($query);

    $user->testing_ans = $count_success_quetion;
    $_SESSION['testing_ans'] = $count_success_quetion;

    $resp['success'] = true;
    return $resp; 
}

function LoginVK(){
    // $client_id = '7339278';
    // $redirect_uri = 'https://cuspate-swamps.000webhostapp.com/test.php';
    // $display = 'popup';  // page mobile
    // $scope = 'email';
    // $response_type = 'code';
    // $auth_uri = "https://oauth.vk.com/authorize?client_id={$client_id}&display={$display}&redirect_uri={$redirect_uri}&scope={$scope}&response_type={$response_type}&v=5.52";
    // $token = json_decode(file_get_contents($auth_uri), true);
    // var_dump($token);

    // https://oauth.vk.com/authorize?client_id=7339278&display=popup&redirect_uri=https://cuspate-swamps.000webhostapp.com/test.php&scope=email&response_type=code&v=5.52"

    // https://cuspate-swamps.000webhostapp.com/orders.php?url=c81e728d9d4c2f636f067f89cc14862c&code=5c6c171d9bbd2891b4

    // $client_id = '7339278'; // ID приложения
    // $client_secret = '8zSaivryhwaMm7Tjjs3V'; // Защищённый ключ
    // $redirect_uri = 'https://cuspate-swamps.000webhostapp.com/orders.php?url=c81e728d9d4c2f636f067f89cc14862c';
    // $url = 'http://oauth.vk.com/authorize';

    // $params = array(
    //     'client_id'     => $client_id,
    //     'redirect_uri'  => $redirect_uri,
    //     'response_type' => 'code'
    // );
    // $auth_uri = $url . '?' . urldecode(http_build_query($params));
    // echo $link = '<p><a href="' . $url . '?' . urldecode(http_build_query($params)) . '">Аутентификация через ВКонтакте</a></p>';
    $token = file_get_contents('https://oauth.vk.com/authorize?client_id=7339278&display=popup&redirect_uri=https://cuspate-swamps.000webhostapp.com/test.php&scope=email&response_type=code&v=5.52');
    var_dump($token);
    exit;
}

function SendOrder(){
    ini_set('display_errors', 0);
    ini_set('display_startup_errors', 0);
    if (!isset($_REQUEST['job_url'])) {
        $resp['message'] = 'Проверьте адрес вакансии';
        $resp['success'] = false;
        return $resp;
    }
    require_once('db.php');
    $result=new db();
    require_once('user.php');
    $user = new User();
    $vakanciya = $user->getJobsByURL($_REQUEST['job_url']);
    if ( $vakanciya['id']==null ) {
        $resp['message'] = 'Вакансия не найдена по данному url';
        $resp['success'] = false;
        return $resp;
    }
    // return $vakanciya;
    $vakanciya_id = $vakanciya['id'];
    $user->RestoreUser();
    $email = $user->email;
    $id_user = $user->id;
    $resume_file = $user->resume_file;
    $resume_file_origin = $user->resume_file_origin;

    $achievements = $user->GetUserAchievementsForMail($id_user, $vakanciya_id);
    $motivation = $user->GetUserMotivation($id_user,$vakanciya_id);

    // $to= "teslenkosasha1992@gmail.com";
    $from = "admin@vrabote.me";
    $to= $vakanciya['email'];
    // $from = $email;
    if ( $to==null ) {
        $resp['message'] = 'Нет указанного имейла получателя заявки в вакансии';
        $resp['success'] = false;
        return $resp;
    }
    if ( $from==null ) {
        $resp['message'] = 'Вы не авторизованы';
        $resp['success'] = false;
        return $resp;
    }
    // $subject = '=?UTF-8?B?' . base64_encode('Тестовое письмо') . '?=';
    $subject = "Заявка"; 
    //  '.$vakanciya['file_name'].'
    //текст письма
    // $message = '
    //     <html><head><title></title></head>
    //         <body>
    //             <p>Вакансия: '.$vakanciya['head'].'</p>
    //             <p>Описание вакансии:</p>
    //             <p>'.$vakanciya['description'].'</p>
    //         </body>
    //     </html>';
    $message = '
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html style="width:100%;font-family:roboto, \'helvetica neue\', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;">
         <head> 
          <meta charset="UTF-8"> 
          <meta content="width=device-width, initial-scale=1" name="viewport"> 
          <meta name="x-apple-disable-message-reformatting"> 
          <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
          <meta content="telephone=no" name="format-detection"> 
          <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i" rel="stylesheet"> 
          <style type="text/css">
        @media only screen and (max-width:600px) {.st-br { padding-left:10px!important; padding-right:10px!important } p, ul li, ol li, a { font-size:16px!important; line-height:150%!important } h1 { font-size:30px!important; text-align:center; line-height:120%!important } h2 { font-size:26px!important; text-align:center; line-height:120%!important } h3 { font-size:20px!important; text-align:center; line-height:120%!important } h1 a { font-size:30px!important; text-align:center } h2 a { font-size:26px!important; text-align:center } h3 a { font-size:20px!important; text-align:center } .es-menu td a { font-size:14px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:16px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:14px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class="gmail-fix"] { display:none!important } .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:block!important } a.es-button { font-size:16px!important; display:block!important; border-left-width:0px!important; border-right-width:0px!important } .es-btn-fw { border-width:10px 0px!important; text-align:center!important } .es-adaptive table, .es-btn-fw, .es-btn-fw-brdr, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0px!important } .es-m-p0r { padding-right:0px!important } .es-m-p0l { padding-left:0px!important } .es-m-p0t { padding-top:0px!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } .es-desk-hidden { display:table-row!important; width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } .es-desk-menu-hidden { display:table-cell!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } }
        .rollover:hover .rollover-first {
            max-height:0px!important;
            display:none!important;
        }
        .rollover:hover .rollover-second {
            max-height:none!important;
            display:block!important;
        }
        #outlook a {
            padding:0;
        }
        .ExternalClass {
            width:100%;
        }
        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
            line-height:100%;
        }
        .es-button {
            mso-style-priority:100!important;
            text-decoration:none!important;
        }
        a[x-apple-data-detectors] {
            color:inherit!important;
            text-decoration:none!important;
            font-size:inherit!important;
            font-family:inherit!important;
            font-weight:inherit!important;
            line-height:inherit!important;
        }
        .es-desk-hidden {
            display:none;
            float:left;
            overflow:hidden;
            width:0;
            max-height:0;
            line-height:0;
            mso-hide:all;
        }
        .es-button-border:hover {
            border-style:solid solid solid solid!important;
            background:#d6a700!important;
            border-color:#42d159 #42d159 #42d159 #42d159!important;
        }
        .es-button-border:hover a.es-button {
            background:#d6a700!important;
            border-color:#d6a700!important;
        }
        td .es-button-border:hover a.es-button-1 {
            background:#018ce9!important;
            border-color:#018ce9!important;
        }
        td .es-button-border-2:hover {
            background:#018ce9!important;
        }
        @media (max-width: 600px){
        .my{
            padding-top: 0px!important;
        }
        }
        </style> 
         </head> 
         <body style="width:100%;font-family:roboto, \'helvetica neue\', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;"> 
          <div class="es-wrapper-color" style="background-color:#F6F6F6;">
           <table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;"> 
             <tr style="border-collapse:collapse;"> 
              <td class="st-br" valign="top" style="padding:0;Margin:0;"> 
               <table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td align="center" bgcolor="transparent" style="padding:0;Margin:0;background-color:transparent;"> 
                   <table bgcolor="transparent" class="es-content-body" align="center" cellpadding="0" cellspacing="0" width="600" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:30px;padding-right:30px;"> 
                       <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td width="540" align="center" valign="top" style="padding:0;Margin:0;"> 
                           <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="center" style="padding:20px;Margin:0;font-size:0;"> 
                               <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                                 <tr style="border-collapse:collapse;"> 
                                  <td style="padding:0;Margin:0px;border-bottom-width:0px;border-bottom-style:solid;border-bottom-color:#CCCCCC;background-image:none;height:1px;width:100%;margin:0px;background-position:initial initial;background-repeat:initial initial;"></td> 
                                 </tr> 
                               </table></td> 
                             </tr> 
                           </table></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="Margin:0;padding-bottom:10px;padding-top:30px;padding-left:30px;padding-right:30px;border-top-left-radius:10px;border-top-right-radius:10px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;background-color:#FFFFFF;" bgcolor="#ffffff"> 
                       <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td width="540" align="center" valign="top" style="padding:0;Margin:0;"> 
                           <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="left" style="padding:0;Margin:0;"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#212121;text-align:center;">Заявочка</h1></td> 
                             </tr> 
                           </table></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" bgcolor="#ffffff" style="padding:0;Margin:0;padding-top:20px;padding-left:30px;padding-right:30px;background-color:#FFFFFF;">
                       <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td width="260" class="es-m-p20b" align="left" style="padding:0;Margin:0;"> 
                           <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="left" style="padding:0;Margin:0;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:22px;font-family:roboto, \'helvetica neue\', helvetica, arial, sans-serif;line-height:33px;color:#016FB9;"><strong>'.$vakanciya['head'].'</strong></p></td> 
                             </tr>
                           </table></td> 
                         </tr> 
                       </table>
                       <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td width="260" align="left" style="padding:0;Margin:0;"> 
                           <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="right" style="padding:0;Margin:0;font-size:0px;"><img class="adapt-img" src="https://cuspate-swamps.000webhostapp.com/uploads/'.$vakanciya['file_name'].'" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;" width="57"></td> 
                             </tr> 
                           </table></td> 
                         </tr> 
                       </table>
                     </td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" bgcolor="#ffffff" style="padding:0;Margin:0;padding-top:20px;padding-left:30px;padding-right:30px;background-color:#FFFFFF;"> 
                       <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td width="540" align="center" valign="top" style="padding:0;Margin:0;"> 
                           <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="left" style="padding:0;Margin:0;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, \'helvetica neue\', helvetica, arial, sans-serif;line-height:24px;color:#4F5A67;"><strong>Описание</strong></p></td> 
                             </tr> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="left" style="padding:0;Margin:0;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, \'helvetica neue\', helvetica, arial, sans-serif;line-height:24px;color:#4F5A67;">'.$vakanciya['description'].'<br><br></p></td> 
                             </tr> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="left" style="padding:0;Margin:0;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, \'helvetica neue\', helvetica, arial, sans-serif;line-height:24px;color:#4F5A67;"><strong>Обязанности</strong></p></td> 
                             </tr> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="left" style="padding:0;Margin:0;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, \'helvetica neue\', helvetica, arial, sans-serif;line-height:24px;color:#4F5A67;"></p></td> 
                             </tr> 
                           </table></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="Margin:0;padding-top:5px;padding-bottom:20px;padding-left:30px;padding-right:30px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:10px;border-bottom-left-radius:10px;background-color:#FFFFFF;" bgcolor="#ffffff">
                       <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td width="258" class="es-m-p20b" align="left" style="padding:0;Margin:0;"> 
                           <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:separate;border-spacing:0px;border-top-left-radius:4px;border-top-right-radius:4px;border-bottom-right-radius:4px;border-bottom-left-radius:4px;" role="presentation"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="center" style="padding:20px;Margin:0;font-size:0;"> 
                               <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                                 <tr style="border-collapse:collapse;"> 
                                  <td style="padding:0;Margin:0px;border-bottom-width:0px;border-bottom-style:solid;border-bottom-color:#CCCCCC;background-image:none;height:1px;width:100%;margin:0px;background-position:initial initial;background-repeat:initial initial;"></td> 
                                 </tr> 
                               </table></td> 
                             </tr> 
                           </table></td> 
                         </tr> 
                       </table> 
                       <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td width="257" align="left" style="padding:0;Margin:0;"> 
                           <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="left" class="es-m-txt-c" style="padding:0;Margin:0;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, \'helvetica neue\', helvetica, arial, sans-serif;line-height:24px;color:#131313;"><br></p></td> 
                             </tr> 
                           </table></td> 
                         </tr> 
                       </table> 
                     </td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="Margin:0;padding-top:30px;padding-bottom:30px;padding-left:30px;padding-right:30px;"> 
                       <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td width="540" align="center" valign="top" style="padding:0;Margin:0;"> 
                           <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="center" style="padding:0;Margin:0;font-size:0;"> 
                               <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                                 <tr style="border-collapse:collapse;"> 
                                  <td style="padding:0;Margin:0px;border-bottom-width:0px;border-bottom-style:solid;border-bottom-color:#CCCCCC;background-image:none;height:1px;width:100%;margin:0px;background-position:initial initial;background-repeat:initial initial;"></td> 
                                 </tr> 
                               </table></td> 
                             </tr> 
                           </table></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="Margin:0;padding-bottom:15px;padding-top:30px;padding-left:30px;padding-right:30px;border-top-left-radius:10px;border-top-right-radius:10px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;background-color:#FFFFFF;" bgcolor="#ffffff"> 
                       <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td width="540" align="center" valign="top" style="padding:0;Margin:0;"> 
                           <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-position:left bottom;" role="presentation"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="center" style="padding:0;Margin:0;"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:roboto, \'helvetica neue\', helvetica, arial, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#016FB9;text-align:left;">Резюме</h1></td> 
                             </tr> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="left" style="padding:0;Margin:0;padding-top:20px;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, \'helvetica neue\', helvetica, arial, sans-serif;line-height:24px;color:#4F5A67;">Добрый день! В данном резюме я отразил все, что умею! Надеюсь,&nbsp;что оцените по достоинству йоу</p></td> 
                             </tr> 
                           </table></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="Margin:0;padding-top:5px;padding-bottom:5px;padding-left:30px;padding-right:30px;border-radius:0px 0px 10px 10px;background-position:left top;background-color:#FFFFFF;"> 
                       <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td width="540" align="center" valign="top" style="padding:0;Margin:0;"> 
                           <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px;padding-left:5px;"><span class="es-button-border es-button-border-2" style="border-style:solid;border-color:#2CB543;background:#FFC80A;border-width:0px;display:inline-block;border-radius:3px;width:auto;background-color:#016FB9;border-top-left-radius:7px;border-top-right-radius:7px;border-bottom-right-radius:7px;border-bottom-left-radius:7px;background-position:initial initial;background-repeat:initial initial;">
                              <a href="https://cuspate-swamps.000webhostapp.com/read_file.php?fn='.$resume_file.'" class="es-button es-button-1" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, \'helvetica neue\', helvetica, arial, sans-serif;font-size:14px;color:#FFFFFF;border-style:solid;border-color:#016FB9;border-width:10px 20px 10px 15px;display:inline-block;background:#FFC80A;border-radius:3px;font-weight:normal;font-style:normal;line-height:17px;width:auto;text-align:center;background-color:#016FB9;border-top-left-radius:7px;border-top-right-radius:7px;border-bottom-right-radius:7px;border-bottom-left-radius:7px;background-position:initial initial;background-repeat:initial initial;">Загрузить резюме кандидата</a></span></td> 
                             </tr> 
                           </table></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="Margin:0;padding-top:30px;padding-bottom:30px;padding-left:30px;padding-right:30px;"> 
                       <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td width="540" align="center" valign="top" style="padding:0;Margin:0;"> 
                           <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="center" style="padding:0;Margin:0;font-size:0;"> 
                               <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                                 <tr style="border-collapse:collapse;"> 
                                  <td style="padding:0;Margin:0px;border-bottom-width:0px;border-bottom-style:solid;border-bottom-color:#CCCCCC;background-image:none;height:1px;width:100%;margin:0px;background-position:initial initial;background-repeat:initial initial;"></td> 
                                 </tr> 
                               </table></td> 
                             </tr> 
                           </table></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table> 


               
               <table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td align="center" style="padding:0;Margin:0;"> 
                   <table bgcolor="transparent" class="es-content-body" align="center" cellpadding="0" cellspacing="0" width="600" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="Margin:0;padding-bottom:10px;padding-top:30px;padding-left:30px;padding-right:30px;border-top-left-radius:10px;border-top-right-radius:10px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;background-color:#FFFFFF;" bgcolor="#ffffff"> 
                       <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td width="540" align="center" valign="top" style="padding:0;Margin:0;"> 
                           <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="left" style="padding:0;Margin:0;"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:roboto, \'helvetica neue\', helvetica, arial, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#016FB9;text-align:left;">Достижения</h1></td> 
                             </tr> 
                           </table></td> 
                         </tr> 
                       </table></td> 
                     </tr>
                     '.$achievements.' 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="padding:0;Margin:0;padding-left:20px;padding-right:20px;background-position:left bottom;"> 
                       <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td width="560" align="center" valign="top" style="padding:0;Margin:0;"> 
                           <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="center" height="24" style="padding:0;Margin:0;"></td> 
                             </tr> 
                           </table></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="padding:0;Margin:0;padding-left:20px;padding-right:20px;background-position:left bottom;"> 
                       <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td width="560" align="center" valign="top" style="padding:0;Margin:0;"> 
                           <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-position:left bottom;" role="presentation"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="center" height="40" style="padding:0;Margin:0;"></td> 
                             </tr> 
                           </table></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
                </table>



                <table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td align="center" style="padding:0;Margin:0;"> 
                   <table bgcolor="transparent" class="es-content-body" align="center" cellpadding="0" cellspacing="0" width="600" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="Margin:0;padding-bottom:10px;padding-top:30px;padding-left:30px;padding-right:30px;border-top-left-radius:10px;border-top-right-radius:10px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;background-color:#FFFFFF;" bgcolor="#ffffff"> 
                       <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td width="540" align="center" valign="top" style="padding:0;Margin:0;"> 
                           <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="left" style="padding:0;Margin:0;"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:roboto, \'helvetica neue\', helvetica, arial, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#016FB9;text-align:left;">Мотивация</h1></td> 
                             </tr> 
                           </table></td> 
                         </tr> 
                       </table></td> 
                     </tr>
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="Margin:0;padding-top:5px;padding-bottom:30px;padding-left:30px;padding-right:30px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:10px;border-bottom-left-radius:10px;background-color:#FFFFFF;"> 
                       <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td width="540" align="center" valign="top" style="padding:0;Margin:0;"> 
                           <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:separate;border-spacing:0px;border-left:1px solid #D4D4D4;border-right:1px solid #D4D4D4;border-top:1px solid #D4D4D4;border-bottom:1px solid #D4D4D4;border-top-left-radius:7px;border-top-right-radius:7px;border-bottom-right-radius:7px;border-bottom-left-radius:7px;" role="presentation"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="left" style="padding:10px;Margin:0;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, \'helvetica neue\', helvetica, arial, sans-serif;line-height:24px;color:#131313;">'.$motivation.'</p></td> 
                             </tr> 
                           </table></td>  
                         </tr> 
                       </table></td> 
                     </tr>


                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="padding:0;Margin:0;padding-left:20px;padding-right:20px;background-position:left bottom;"> 
                       <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tbody><tr style="border-collapse:collapse;"> 
                          <td width="560" align="center" valign="top" style="padding:0;Margin:0;"> 
                           <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                             <tbody><tr style="border-collapse:collapse;"> 
                              <td align="center" height="24" style="padding:0;Margin:0;"></td> 
                             </tr> 
                           </tbody></table></td> 
                         </tr> 
                       </tbody></table></td> 
                     </tr>
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="padding:0;Margin:0;padding-left:20px;padding-right:20px;background-position:left bottom;"> 
                       <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tbody><tr style="border-collapse:collapse;"> 
                          <td width="560" align="center" valign="top" style="padding:0;Margin:0;"> 
                           <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-position:left bottom;" role="presentation"> 
                             <tbody><tr style="border-collapse:collapse;"> 
                              <td align="center" height="40" style="padding:0;Margin:0;"></td> 
                             </tr> 
                           </tbody></table></td> 
                         </tr> 
                       </tbody></table></td> 
                     </tr>

                   </table></td> 
                 </tr> 
                </table>

               </td> 
             </tr> 
           </table> 
          </div>  
         </body>
        </html>';
    
    // название файла
    $filename = $resume_file_origin; //"2_Resume.txt";
    // месторасположение файла
    $filepath = "../files/".$resume_file; //"../files/2_Resume.txt";
    // генерируем разделитель
    $boundary = "--".md5(uniqid(time())); 
    // разделитель указывается в заголовке в параметре boundary 
    $mailheaders = "MIME-Version: 1.0;\r\n"; 
    $mailheaders .="Content-Type: multipart/mixed; boundary=\"$boundary\"\r\n"; 
    $mailheaders .= "From: $from <$from>\r\n"; 
    $mailheaders .= "Reply-To: $from\r\n"; 
    // первая часть само сообщение
    $multipart = "--$boundary\r\n"; 
    $multipart .= "Content-Type: text/html; charset=windows-1251\r\n";
    $multipart .= "Content-Transfer-Encoding: base64\r\n";    
    $multipart .= "\r\n";
    $multipart .= chunk_split(base64_encode(iconv("utf8", "windows-1251", $message)));
    // Закачиваем файл 
    $fp = fopen($filepath,"r"); 
    if ($fp) { 
        // print "Не удается открыть файл."; 
        // exit();
        $file = fread($fp, filesize($filepath)); 
        fclose($fp);
        // второй частью прикрепляем файл, можно прикрепить два и более файла
        $message_part = "\r\n--$boundary\r\n"; 
        $message_part .= "Content-Type: application/octet-stream; name=\"$filename\"\r\n";  
        $message_part .= "Content-Transfer-Encoding: base64\r\n"; 
        $message_part .= "Content-Disposition: attachment; filename=\"$filename\"\r\n"; 
        $message_part .= "\r\n";
        $message_part .= chunk_split(base64_encode($file));
        $message_part .= "\r\n--$boundary--\r\n";
        $multipart .= $message_part;
    }
    // отправляем письмо
    $success = mail($to,$subject,$multipart,$mailheaders);
    if (!$success) { $message = 'Ошибка при отправке почты'; }
    $to = "admin@vrabote.me";
    $success = mail($to,$subject,$multipart,$mailheaders);
    if (!$success) { $message = 'Ошибка при отправке почты'; }
    $to= "teslenkosasha1992@gmail.com";
    $success = mail($to,$subject,$multipart,$mailheaders);
    if (!$success) { $message = 'Ошибка при отправке почты'; }
    // ответ
    $resp['to'] = $to;
    $resp['from'] = $from;
    $resp['message'] = $message;
    $resp['success'] = $success;
    return $resp;
}

function UploadFileResume(){
    require_once('db.php');
    $result=new db();
    require_once('user.php');
    $user = new User();
    $user->RestoreUser();
    $files = $_FILES; // полученные файлы
    $uploaddir = '../files';
    $file_origin_name = '';
    $id_user = $user->id;
    $resp = array();
    foreach( $files as $type => $file ){
        $datetime = date('Y-m-d H:i:s',time());
        // $datetimecode = date('YmdHis',time());
        $file_name_temp = str_replace(['"',"'"], ['',''], $file['name']);
        $file_origin_name = $file['name'];
        $ext = explode('.', $file['name']);
        if ($id_user) {
            $file_name = $id_user."_".$file_origin_name; //.'.'.$ext[1];
            if( !move_uploaded_file( $file['tmp_name'], "$uploaddir/$file_name" ) ){ $resp['success'] = false; return $resp; }
            $query = "UPDATE `users` SET `resume_file`='$file_name', `resume_file_origin`='$file_origin_name' WHERE `id`=$id_user;";
            $res = $result->updateDataBase($query);
            $user->resume_file = $file_name;
            $user->resume_file_origin = $file_origin_name;
            // $user->Serialize();
            $_SESSION['resume_file'] = $file_name;
            $_SESSION['resume_file_origin'] = $file_origin_name;
            // $user->RestoreUser();
        }
    }
    $resp['success'] = true;
    return $resp;
}

function Registr() {
    require_once('db.php');
    $result = new db();
    $resp = array();
    require_once('user.php');
    $user = new User();

    $name = trim($_REQUEST["name"]);
    $secondname = trim($_REQUEST["secondname"]);
    $email = trim($_REQUEST["login"]);
    $pass = md5($_REQUEST["pass"]);

    if($user->LoginUser($email, $_REQUEST["pass"]) == true) {
        $resp['success'] = false;
        $resp['message'] = 'Данный E-mail уже используется в системе';
        return $resp;
    }

    $query = "INSERT INTO `users` (`login`,`password`,`name`,`surname`,`email`,`role`,`date_login`,`hash_email`) 
        VALUES ('$email','$pass','$name','$secondname','$email',0,NOW(),MD5('$email'));";
    $insert_id = $result->insertDataBase($query);
    
    $resp['insert_id'] = $insert_id;
    $resp['query'] = $query;
    $resp['success'] = true;
    return $resp;
}

function UploadFile(){
    require_once('db.php');
    $result=new db();
    $files = $_FILES; // полученные файлы
    $uploaddir = '../uploads';
    $file_origin_name = '';
    $idjob = trim($_REQUEST["idjob"]);
    $resp = array();
    foreach( $files as $type => $file ){
        $datetime = date('Y-m-d H:i:s',time());
        $datetimecode = date('YmdHis',time());
        $file_name_temp = str_replace(['"',"'"], ['',''], $file['name']);
        $file_origin_name = $file['name'];
        $ext = explode('.', $file['name']);
        $file_name = $idjob."_".$datetimecode.'.'.$ext[1];
        if( !move_uploaded_file( $file['tmp_name'], "$uploaddir/$file_name" ) ){ $resp['success'] = false; return $resp; }
        if ($idjob) {
            $query = "INSERT INTO `import_files` (`origin_file_name`,`file_name`,`id_job`) VALUES ('$file_origin_name','$file_name',$idjob);";
            $last_id = $result->insertDataBase($query);
            $query = "UPDATE `jobs` SET `file`=$last_id WHERE `id`=$idjob;";
            $res = $result->updateDataBase($query);
        }
    }
    $resp['success'] = true;
    return $resp;
}

function DeleteJob() {
    require_once('db.php');
    $result = new db();
    $resp = array();
    $jobhead = trim($_REQUEST["jobhead"]);
    $idjob = trim($_REQUEST['idjob'])=='' ? false : intval($_REQUEST['idjob']);
    if ($idjob) {
        $query = "UPDATE `jobs` SET `delete`=1 WHERE `id`=$idjob;";
        $res = $result->updateDataBase($query);
    }
    $resp['success'] = $res;
    return $resp;
}

function SaveJob() {
    require_once('db.php');
    $result = new db();
    $resp = array();
    $idnewjob = 0;

    $jobhead = trim($_REQUEST["jobhead"]);
    $jobcoment = trim($_REQUEST["jobcoment"]);
    $company = trim($_REQUEST["jobcompany"]);
    $redirect_url = trim($_REQUEST["redirect_url"]);
    $idjob = trim($_REQUEST['idjob'])=='' ? false : intval($_REQUEST['idjob']);
    $email = trim($_REQUEST["email"]);

    if ($idjob) {
        $query = "UPDATE `jobs` SET `head`='$jobhead',`description`='$jobcoment',`email`='$email',`company`='$company',`redirect_url`='$redirect_url' WHERE `id`=$idjob;";
        $result->updateDataBase($query);
    }else{
        $query = "INSERT INTO `jobs` (`head`,`description`,`email`,`company`,`redirect_url`) VALUES ('$jobhead','$jobcoment','$email','$company','$redirect_url')";
        $idnewjob = $result->insertDataBase($query);
    }

    if (!$idjob) { $idjob=''; }
    $resp['idjob'] = $idjob;
    $resp['idnewjob'] = $idnewjob;
    $resp['query'] = $query;
    $resp['success'] = true;
    return $resp;
}

function Login() {
    require_once('user.php');
    $resp = array();

    $login = trim($_REQUEST["login"]);
    $pass = $_REQUEST["pass"];

    $user = new User();
    if($user->LoginUser($login, $pass) == true) {
        $resp['success'] = true;
        $resp['message'] = $user->GetMessage();
        $resp['role'] = $user->role;
        $resp['email'] = $user->email;
    } else {
        $resp['success'] = false;
        $resp['message'] = $user->GetMessage();
    }
    return $resp;
}

function Logout() {
    require_once('user.php');
    $resp = array();

    $user = new User();
    $user->LogoutUser();

    $resp['success'] = true;
    $resp['message'] = $user->GetMessage();

    return $resp;
}

function logemail($text,$message){
	$myfunction=new myfunction();
	$dir="logs";
	$myfunction->writeToLogFile(' -> '.date('Y-m-d H:i:s').' <> '.$text.' <> '.$message, 'ajax_send_email' ,$dir);
}

?>