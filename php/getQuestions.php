<?php
    /* PHP Excel - Read a simple 2007 XLSX Excel file */

    /** Set default timezone (will throw a notice otherwise) */
    date_default_timezone_set('America/Los_Angeles');
    header('Access-Control-Allow-Origin: *'); 
    include 'PHPExcel/Classes/PHPExcel/IOFactory.php';
    $inputFileName = '../test_Excel/questions.xlsx';

    //  Read your Excel workbook
    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFileName);
    } catch (Exception $e) {
        die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
    }

    //  Get worksheet dimensions
    $sheet = $objPHPExcel->getSheet(0);
    $highestRow = $sheet->getHighestRow();
    $highestColumn = $sheet->getHighestColumn();

    // echo 'highestRow '.$highestRow.' highestColumn '.$highestColumn.'<br>';
    // $array_parse = $arrayName = array('' => , );
    $array_parse = array();
    $question_details  = array();
    $i = 0;
    $count_question = 1;
    $arrayKey = array(0 => 'number',1 => 'title',2 => 'type',3 => 'variantsCount',4 => 'variants',5 => 'ansver');
    // var_dump($arrayKey); echo "<br>";
    //  Loop through each row of the worksheet in turn
    for ($row = 1; $row <= $highestRow; $row++) {
    // for ($row = 1; $row <= 18; $row++) {
        //  Read a row of data into an array
        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
        // var_dump($rowData[0]); echo "<br>";
        foreach($rowData[0] as $k=>$v) {
            if (!empty($v)) {
                if ($arrayKey[$i]=='variants') {
                    // exit;
                    if ($v=='variants') { continue; }
                    if (isset($question_details[$arrayKey[$i]])) {
                        $question_details[$arrayKey[$i]] .= ';;'.$v;
                    }else{
                        $question_details[$arrayKey[$i]] = $v;
                    }
                }elseif ($arrayKey[$i]=='ansver') {
                    if ($v=='ansver') { continue; }
                    if (isset($question_details[$arrayKey[$i]])) {
                        $question_details[$arrayKey[$i]] .= ';;'.$v;
                    }else{
                        $question_details[$arrayKey[$i]] = $v;
                    }
                }else{
                    $question_details[$arrayKey[$i]] = $v;
                }
                // echo '$arrayKey[$k]'.$arrayKey[$i].' v '. $v."<br />";
            }
        }
        $i++;
        if ($i==6) {
            $row++;
            $i = 0;
            if (count($question_details)==0) { continue; }
            $array_parse[$count_question] = $question_details;
            $question_details  = array();
            $count_question++;
        }
    }
    $count_question--;
    // echo "string";
    // echo " array_parse <br>";var_dump($array_parse); echo "<br>";
    echo json_encode(array('success'=>true, 'highestRow'=>$highestRow, 'count_question'=>$count_question, 'array_parse'=>$array_parse));
    // $resp['success'] = true;
    // var_dump($resp);
    // return $resp;
?>